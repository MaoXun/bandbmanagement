function testdn() {

    $.ajax({
        url: "http://localhost:60225/api/Report/exportExcel?first=2021-05-01&last=2021-07-31&year=2021&month=08",
        type: 'POST',
        xhrFields: {
            // 將回傳結果以 Blob ，也就是保持原本二進位的格式回傳
            responseType: "blob"
        },
        success: function (response) {
            console.log(response);
            if (response.size == 0) {
                alert("選取錯誤!");
                return;
            }
            //.done() 類似於 ajax success
            //這邊一定要用原生的 document.createElement
            //jQuery 沒辦法真的模擬原生的 click event
            //$ --> JQuery物件
            const $a = document.createElement("a")
            // 給下載回來的資料產生一個網址
            const url = URL.createObjectURL(response)
            // 設定下載的檔名
            let date = Date.now();
            $a.download = `${date.toString()}_Report.xlsx`
            // 設定網址
            $a.href = url
            // 模擬使用者按下連結
            $a.click()
            // 5 秒後清除產生的網址，這時候使用者應該按下了下載的按鈕了
            // 不清除的話會造成記憶體的浪費，這不會中斷使用者的下載
            // 不過若你下載完就要跳轉到下一頁的話，其實這個可以不用
            setTimeout(() => URL.revokeObjectURL(url), 5000)

        }, error: function (xhr) {
            alert('Ajax request 發生錯誤');
        }
    })


    // $.ajax({
    //     url: "http://localhost:60225/api/Report/exportExcel?first=2021-05-01&last=2021-07-31&year=2021&month=08",
    //     type: 'POST',
    //     xhrFields: {
    //         // 將回傳結果以 Blob ，也就是保持原本二進位的格式回傳
    //         responseType: "blob"
    //     }
    // }).done((data) => {
    //     console.log(data);
    //     //.done() 類似於 ajax success
    //     // 這邊一定要用原生的 document.createElement
    //     // jQuery 沒辦法真的模擬原生的 click event
    //     //$ --> JQuery物件
    //     // const $a = document.createElement("a")
    //     // // 給下載回來的資料產生一個網址
    //     // const url = URL.createObjectURL(data)
    //     // // 設定下載的檔名
    //     // let date = Date.now();
    //     // $a.download = `${date.toString()}_Report.xlsx`
    //     // // 設定網址
    //     // $a.href = url
    //     // // 模擬使用者按下連結
    //     // $a.click()
    //     // // 5 秒後清除產生的網址，這時候使用者應該按下了下載的按鈕了
    //     // // 不清除的話會造成記憶體的浪費，這不會中斷使用者的下載
    //     // // 不過若你下載完就要跳轉到下一頁的話，其實這個可以不用
    //     // setTimeout(() => URL.revokeObjectURL(url), 5000)
    // })
}

//將 base64 文字轉換成 byte 陣列才可以儲存到 Blob 物件
function base64ToArrayBuffer(base64) {

    //window.atob 在 IE 瀏覽器不支援
    var binaryString = window.atob(base64);

    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

function testdownload() {
    const url = base64toURL();

    const $a = document.createElement("a")
    // 設定下載的檔名
    let date = Date.now();
    $a.download = `${date.toString()}_Report.xlsx`
    // 設定網址
    $a.href = url
    // 模擬使用者按下連結
    $a.click()
    // 5 秒後清除產生的網址，這時候使用者應該按下了下載的按鈕了
    // 不清除的話會造成記憶體的浪費，這不會中斷使用者的下載
    // 不過若你下載完就要跳轉到下一頁的話，其實這個可以不用
    setTimeout(() => URL.revokeObjectURL(url), 5000)
}

function base64toURL() {

    let str = `UEsDBBQABgAIAHCL81LkSK2vGAEAADMDAAATABQAW0NvbnRlbnRfVHlwZXNdLnhtbJmZEAAAAAAAAAAAAAAAAAAAAAAAtZLPSgMxEMZfZclVmrQeRKTbHqoeVbA+wJjMdkPzj8y0tm9vNisipYIeepok38z3/QgzXx68a/aYycbQipmcigaDjsaGTSve1o+TW9EQQzDgYsBWHJHEcjFfHxNSU2YDtaJnTndKke7RA8mYMBSli9kDl2veqAR6CxtU19PpjdIxMAae8OAhFvN77GDnuFmN74N1KyAlZzVwwVLFTDQPhyKOlMNd/WFuH8wJzOQLRGZ0tYd6m+jqNKCoNCQ8l4/J1uC/ImLXWY0m6p0vI5JSRjDUI7J3slbpwYYx9AUyP4Evrurg1EfM2/cYt7JqFwEYIur5t/wqkqpldkEQ4qNDOkcxKpeM7iGjeeVclvw8wc+GbxBVl37xCVBLAwQUAAYACABwi/NSmNrri64AAAAnAQAACwAUAF9yZWxzLy5yZWxzmZkQAAAAAAAAAAAAAAAAAAAAAACNz8EOgjAMBuBXWXqXgQdjDIOLMeFq8AHmVgYB1mWbCm/vjmI8eGz69/vTsl7miT3Rh4GsgCLLgaFVpAdrBNzay+4ILERptZzIooAVA9RVecVJxnQS+sEFlgwbBPQxuhPnQfU4y5CRQ5s2HflZxjR6w51UozTI93l+4P7TgK3JGi3AN7oA1q4O/7Gp6waFZ1KPGW38UfGVSLL0BqOAZeIv8uOdaMwSCrwq+ebB6g1QSwMEFAAGAAgAcIvzUhiSaBPWAAAAUgEAAA8AFAB4bC93b3JrYm9vay54bWyZmRAAAAAAAAAAAAAAAAAAAAAAAI1PQU7EMAz8SuQ7m8IBoartHuCyEhJISNxD6myjTeLKSVn+xoEn8QWcrSo4cvKM7RmPvz+/uv1HDOodOXtKPVzvGlCYLI0+HXtYiru6g/3QnYlPb0QnJdspt9zDVMrcap3thNHkHc2YZOaIoylC+ajJOW/xgewSMRV90zS3mjGYIpfy5OcMq9t/vPLMaMY8IZYYVqtofIKhq6lePZ7zb8hKlR46/Wd2kW5VJROxh5eK5d9L7zDK96C49QL4MAquFpvOmmCfWbklhHuBT+mRzKqoW9vl4QdQSwMEFAAGAAgAcIvzUoFikqLWAAAANAIAABoAFAB4bC9fcmVscy93b3JrYm9vay54bWwucmVsc5mZEAAAAAAAAAAAAAAAAAAAAAAArZHPasMwDIdfxei+OOlgjFG3lzHotX8eQNhKHJrYxtLa5e1rNlZSKGOHnoRk9P0+rOX6axzUiTL3MRhoqhoUBRtdHzoDh/3H0ysoFgwOhxjIwEQM69VySwNKWWHfJ1aFEdiAF0lvWrP1NCJXMVEoL23MI0ppc6cT2iN2pBd1/aLznAG3TLVxBvLGNaD2U6L/sGPb9pbeo/0cKcidCH2O+cieSAoUc0di4Dpi/V2aqlBB35dZPFKGZRrKX15Nfvq/4p8fGu8xk9tJLoeeW8zHvzL65tqrC1BLAwQUAAYACABwi/NSTAqNYn0HAAA9KwAAGAAUAHhsL3dvcmtzaGVldHMvc2hlZXQxLnhtbJmZEAAAAAAAAAAAAAAAAAAAAAAAjVpJcuNGEPwKg/cR0AU0FoakiRgJK3WY8ME+cyhIYgxJKEBq8dt88JP8BQMESHVXYumLrcnKagCZvVSB+O+ff6+/f+62s/eiOmzK/c1cXNnzWbFfl4+b/fPN/O349C2Yf7+9/iir34eXojjOavr+sKhu5i/H4+vCsg7rl2K3OlyVr8W+jj2V1W51rP9ZPVvl09NmXdyX67ddsT9aZNueVRXb1bG+1OFl83qYt6OZjHV4rYrV4+kWdtt2qN1qs+9GWHziDe0266o8lE/Hq3W56+4FhhFufUfvm+bhz0Pt1ib3s1tVv99ev9VDv9bP82uz3Rz/Pt3WfLZbL7LnfVmtfm2Lm/lnNb+9ftzUCjQXmVXF0838h1g8OLXQ1u316Vb+3BQfB+XvWSP3r7L83fwje7yZt1wLyPHpjn5Ws8fiafW2Pf5RfqTF5vnlWDspTznrcns4/Xe229T+Un17q8/T/z82j8eXmlff8frtcCx3f3XA3FISnC7BuSQEo3y347sXPrmjCbJLkKYJXpfgGd6R3/F900cOuoTgK4FGE8IuITS9grC7jOYPs2sIcU4Rl5RmCo2lnM0WNJFitbPkNKXuV8fV7XVVfsyqJjpr5tJX1mV21ZN63TB+1JRDe1c1fKjh91v72npvBu0o9x2FTnfXIFGH2EqS0JNySFr2JJGe9KAmWfUzXB6E2gehkQehNlcZ3dFHv2sZpDBc9qTIkDojQoanM2Jk+DojQUagM1JkhDojQ4ZgtuUtxVEpzKRlp5nG4Z50HBc8caY9cU65Uh2em9JSXJXCXemhcFvOlNOksm0mRayFmRsslTkxGs3UqMujeRv12lwIL3vEYXPpQRtBkPiaSZoT7rQTLl6MTcs7F2Vm8/K+h8ImZuQqkgh0QgszJ9QY92Eklo3EcldREG9nibIQozyoI3iB3e+AnHZA4qWYAHdyei30ULgDcliOWI3xbUmNBSBVOjJsNp6aS0VCDC97lOGbkGQ2DtjgTdvgTdvgTdvQQ+E2eOMLwRtZCJ6+6XAnWJh5oUUhnHuKkg6El21YaPqwLfuh5YTtECIIB3Ylf9oMf9oMf9oMpBCjRP6oorEWZmaoMYlm+KNmaMlohq+Y4aMZPh7PxE6+B3/oeA6m5Q+m5Q+m5e+h8LUQjK+FYGQtBONrIRiVPxhfC8H4WuiRhx/Q6ghC2HJgXwqnvQjxYvyADnGe8wO6h8K9CMe9UMPM6USNOegFCzMvtCiE81D1AsJLlMfhB7U6gjd0QjTtx5QVDYdfjK+LjqMq7bCd476HAyvjwhnYmbQ4XxtasMcQHmeO6GG0pIu3ikr0pE8nflZoY7hy0BZhYIswsEUY2NLD4efFhTNkixqX3BYtiD2FFu+xRUuXaItQTw2IL/t04qeGNoagQVsM+m5BeDmP20IGtiAH9q4LZ8gWLc5toYnVQuO20MRqIX3/AVt6dPK5LeoYwqchWwxab4HtpRNwW7CzdkJuC3Jcm9ui9sE9Z4oWd7gtrIkGW9Q4mDKenAu1g8bqbdmjkiu4KY5u7IAnBk24OLeb1sWBc+9qXfTmSARIDEgCSApIBkguzr3t1zs6bIhdeEmndsTuUMkjDFpiIUEPCXpwJAIkBiQBJAUkAyQXEvTA3tSFI05rTt3BCWLQnAoPBPFAEI5EgMSAJICkgGSA5MIDQbB/dl0uiKcJohw+uiAGDaLwQRAfBOFIBEgMSAJICkgGSC58EAR7WBdOW7XPCxyPBgQxaNlEAIIEIAhHIkBiQBJAUkAyQHIRgCDYNrm8bRJa3yTDoRli0DeJEAQJQRCORIDEgCSApIBkgOQiBEGwd3Hh4Neal8FDhgy6F7K5IGRzQQCJAIkBSQBJAckAycnmgnTI+EumjtS9ZQqlGFg0ZNA5kABJBEjCkQiQGJAEkBSQDJCcBEjS/fKlvupxAy5JR8J3PWTy+xiBCAQicCQCJAYkASQFJAMkJwIRsELmv1g9kFohB+QPlR9kUCKTA4o4oAhHIkBiQBJAUkAyQHJyQJGe34jg9zm1PB15HUsGBSpBgUpQoAISARIDkgCSApIBkhMUqIQFKrwSJbVAHds6DEpUghKVoEQFJAIkBiQBJAUkAyQnKFGpp0QNuSJaiSocpcfRFTGoUQlqVIIaFZAIkBiQBJAUkAyQnKBG7RD1UwDJXxaeOZ0knhx8NUUGZSpBmUpQpgISARIDkgCSApIBkhOUqYRlqoS9Vfs5Qnh0FfpSBjJ0/IECjQwqVoKKlaBiBSQCJAYkASQFJAMkJ6hYCStWCbusWrG6dlhr43l+6AWD0hjUrgS1K0HtCkgESAxIAkgKSAZITlC7EtauEio1tXYN5JUt61OZfNeTAdnOwE7jGFSyTlsC+urFWa9513GCL8EAiS5I2230fB6iEvhLx0SLOhI+5Ej1dBg+0wfwez4UUWtdSQI/FTGpmLVR+jZ5S/lAbFdUz8VdsW2+G7v83X3d6NiL++7zRh4Si+ZrMAxEYpH3Bpa0aL4g6gn4i+a3y74MsTiVun0hb3HauXtCdVKbY6mP9lKsHosqLstjUZ2Clw9hb/8HUEsDBBQABgAIAHCL81ILSJVulgIAAIAVAAANABQAeGwvc3R5bGVzLnhtbJmZEAAAAAAAAAAAAAAAAAAAAAAA3VjLjtMwFP0Vy3smTefBgJKMAKkSmxHSsGDrJE5iybEjxxm18xdI7FmwRPADleBrQMyKX8CPtHUfKNOZTqHJJs7Jvff43FjX1/k9/R5cjEsKrrGoCWch9I8GEGCW8JSwPISNzJ6cw4soqOWE4qsCYwmUPatDWEhZPfe8OilwieojXmGm3mRclEiqR5F7dSUwSmvtVFJvOBiceSUiDEYBa8pRKWuQ8IbJECpKLwoyzhbQCbSAYr4B14iqmfnGiqESW+AVoiQWxKCete3w+PHt66/px59fPt1O399+/tDpmXDKBRB5HMKRugbqgkCSxZS3CB1r5D4zM7dahSGUztMzhBaIggpJiQUbqQfQjt9OKhxCxhlu4xjDDvNcoIk/PHU9zE0xx1ykan3MuH39bSwWBRRnUrsIkhdmIHmlbzGXkpd6lBKUc4aoCTxzW3I3ayuEslBrYxFrFdWBWyzlTUytuJZn1fjOpE6ofdJu1uoQ7JN2k9Y5ts8E74x0m/T+T0qdEPv7prsj3SrB96Zdq8u6Mi/XZU87PawobUzoXaiN10Pq4No62n05eNTC0w7UxpFgSq+0y7tssXuoJI4zYDuA16lJm97h2qEO5LrZIB3+qKro5LIpYyxGpv/QZm5UYzDi1h+MMxd+I7jEibTdj5nAduH91fBWvn41bF+9NEg3t8FeUJKzEs+0otkjKLggN4pE9wuJArCAum+TJHERncBxtkMRfh9EHPdBxEkfRJz2QcRZH0Q87YOI84MRMfy7iGcHI+J4VcShzNPZzwYHk+21zsVRse22vMu+xu9HY9OLpuDxqrg5Ieyl/v3LD+G15wvntLJ01pijQP+tC+GlVkadCcYNoeoAuOn8ooKm42zlN6e3+JUa/QFQSwMEFAAGAAgAcIvzUkFtk4gNAwAADAcAABQAFAB4bC9zaGFyZWRTdHJpbmdzLnhtbJmZEAAAAAAAAAAAAAAAAAAAAAAAjVXdThpREH6VzSZN8EL27K6AGsDENr1v0j4A0a2SyGLZ1bQ3jUgpCyhgS7HpgkrFin8oihWF1ZfhnF2ueIUOYtuEsyYmJBtmvpn5vjlz5vRad96p96EFZlmKKMGw7GN5J2IZSZ4JzwblOR/75vXL0XGWUdSAPBtYCMuSj/0gKSwz5fcqispAqKz42HlVXZzkOGVmXgoFFGd4UZLB8zYcCQVU+BuZ45TFiBSYVeYlSQ0tcAJCbi4UCMosMxNeklUf63KxzJIcfLckPf9ngBJBv1f1d4wdom2YPz6ZerMbTYgvcPKSfMqSsw1cu/Nyqt/L9YEDsIAEHl83kIsUNcSTzT3mI/PX5gGbeG+zqqu4UMM7datcfSzDOKDNxDbZO7HH4fhex8hCNlLcHvaR79s2VqjaTWzgtVMItY8bIBwAIce3I8Pezs0N+dYctprZW4gh+d+QleKY2sGxa7Kftg8y9Uq3tUX57kV3y+vDjm7qyjqvEi1HisfDPuThBIEqr+s0DtrKj4N8Snvh0LpIWOctM1914JUj3oMQIvrtmMB7qFZAOTRB9yc/bBqlNKxESWPTLt04xb54Zu5TR4RzmlXVbGUht42sQdMGsqgxKdXNXNLSrs09kByvigi3L/F6iocPLdnNIQ8lublC4/pUxmyokM2DbiXXa2u8AAV67aRdCZ7u6tqwaXr6FWSxSr/M+gWkMRNfzFiFd3UMamTu76GI7MgYtV47gytFfJaBZMLYI5RcnOCyF0Iah/jQgJ9dDD2L8RNbcoJoR+6yRgzdIYIi6hxw9shqpR7m1GonrfIaKWVoWC6GCyUzc0Bg1+hbJLnrsekPacaJkYdM+GuUmhy9BVV4xI3yAodE+uhPrOwpgDp3ZSrtwTZwdODaT9gZvAtnz2h+2gExKlj7TLZyNhugG02TqyqAqGu6WoaxhTizER/y9Z+DSWUxMAOvBOx7RYosS6zfypetdJ0U1hiHW3Ty7mcjzJPiuq0YLqZNXWccotuJJp4aZ1Xr/a7u7jMO5PS46LCHKzz538zBW+b/A1BLAQItABQABgAIAHCL81LkSK2vGAEAADMDAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0AFAAGAAgAcIvzUpja64uuAAAAJwEAAAsAAAAAAAAAAAAAAAAAXQEAAF9yZWxzLy5yZWxzUEsBAi0AFAAGAAgAcIvzUhiSaBPWAAAAUgEAAA8AAAAAAAAAAAAAAAAASAIAAHhsL3dvcmtib29rLnhtbFBLAQItABQABgAIAHCL81KBYpKi1gAAADQCAAAaAAAAAAAAAAAAAAAAAF8DAAB4bC9fcmVscy93b3JrYm9vay54bWwucmVsc1BLAQItABQABgAIAHCL81JMCo1ifQcAAD0rAAAYAAAAAAAAAAAAAAAAAIEEAAB4bC93b3Jrc2hlZXRzL3NoZWV0MS54bWxQSwECLQAUAAYACABwi/NSC0iVbpYCAACAFQAADQAAAAAAAAAAAAAAAABIDAAAeGwvc3R5bGVzLnhtbFBLAQItABQABgAIAHCL81JBbZOIDQMAAAwHAAAUAAAAAAAAAAAAAAAAAB0PAAB4bC9zaGFyZWRTdHJpbmdzLnhtbFBLBQYAAAAABwAHAMIBAABwEgAAAAA=`;

    var blob = new Blob([base64ToArrayBuffer(str)], { type: 'application/octet-stream' });
    console.log(blob);
    return URL.createObjectURL(blob);
}

function testAJAX() {
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Customer/list',
        type: 'GET',
        async: false,
        error: function (xhr) {
            alert("AJAX發生錯誤！");
        },
        success: function (response) {
            console.log(response);
        }
    });
}