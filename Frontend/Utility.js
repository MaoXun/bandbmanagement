var span_recorder = {
    "product": false,
    "package": false
}; // 同一時間點 只會有一個選項被觸發

$(document).ready(
    function pageLoad() {
        //判斷展開選項是否點擊
        let current_span_recorder = JSON.parse(sessionStorage.getItem('span_recorder')); //載入重整前 沒拿到就是null
        sessionStorage.setItem('span_recorder', JSON.stringify(span_recorder));
        for (let key in current_span_recorder) //json物件foreach寫法
        {
            //console.log(key + " is " + current_span_recorder[key]);
            const span_option = document.getElementById('span_' + key);
            // console.log(span_option);
            const span_content = span_option.parentNode.id; //取得父節點
            const spandiv = document.getElementById(span_content).previousElementSibling.id; //取得(上個)兄弟節點

            if (current_span_recorder[key]) { //維持打開
                clickSpan(spandiv, span_content);
                span_option.classList.add('active-span-option');
            } else {
                span_option.classList.remove('active-span-option');
            }
        }
    }
);

function clickOption(id) {
    // let options = document.getElementsByClassName("option");
    // let target = document.getElementById(id);
    // for (let i = 0; i < options.length; i++) {
    //     options[i].classList.remove("active-option");
    // }
    // target.classList.add("active-option");
    window.location.href = `./${id}.html`;
}

function clearText(willClearText) {
    willClearText.value = "";
    location.reload();
}

function collapse() {
    let isCollapsed = false;
    let iconinfos = document.getElementsByClassName("iconinfo");
    let navbar = document.getElementById("navbar");
    let options = document.getElementsByClassName("option");

    if (iconinfos[0].classList.contains("collapse")) {
        isCollapsed = true;
        //console.log(isCollapsed);
    }

    //console.log(iconinfos);
    if (!isCollapsed) { //未收合
        for (let i = 0; i < iconinfos.length; i++) {
            iconinfos[i].classList.remove("opentext"); //絕對不能remove iconinfo!! 否則iconinfos本身會變  要另外新增一個class 
            iconinfos[i].classList.add("collapse");    // iconinfo 當作取得元素的class 
            options[i].classList.add("closeoption")
        }
        navbar.classList.add("closenavbar");
        sessionStorage.setItem('isCollapsed', !isCollapsed);
    } else { //已收合
        for (let i = 0; i < iconinfos.length; i++) {
            iconinfos[i].classList.remove("collapse");
            iconinfos[i].classList.add("opentext");
            options[i].classList.remove("closeoption")
        }
        navbar.classList.remove("closenavbar");
        sessionStorage.setItem('isCollapsed', !isCollapsed);
    }
}

let isSpanned = false;
function clickSpan(spandiv_id, spancontent_id) {
    if (isSpanned) {
        closeSpan(spandiv_id, spancontent_id);
        isSpanned = false;
    } else {
        spanOption(spandiv_id, spancontent_id);
        isSpanned = true;
    }
}

function spanOption(spandiv_id, spancontent_id) {
    const spanicondiv = document.getElementById(spandiv_id);
    spanicondiv.innerHTML = '';
    spanicondiv.innerHTML += `<i class="fas fa-sort-down"></i>`;

    const span_content = document.getElementById(spancontent_id);
    span_content.classList.remove("span_content");
    span_content.classList.add("active-span");
}

function closeSpan(spandiv_id, spancontent_id) {
    const spanicondiv = document.getElementById(spandiv_id);
    spanicondiv.innerHTML = '';
    spanicondiv.innerHTML += `<i class="fas fa-caret-right"></i>`;

    const span_content = document.getElementById(spancontent_id);
    span_content.classList.remove("active-span");
    span_content.classList.add("span_content");

}

function clickProduct() {
    clickSpan('product_spanicondiv', 'product_span_content');
    span_recorder["product"] = true;
    sessionStorage.setItem("span_recorder", JSON.stringify(span_recorder));
}

function clickPackage() {
    clickSpan('product_spanicondiv', 'product_span_content');
    span_recorder["package"] = true;
    sessionStorage.setItem("span_recorder", JSON.stringify(span_recorder));
}

function goBack() {
    document.getElementById("pagecontent").innerHTML = previousHTML;
    document.getElementById("pagecontent").style = previousStyle;
}