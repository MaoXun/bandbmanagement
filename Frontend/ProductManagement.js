var page_num = 0;
var global_products = []; //List

$(document).ready(
    function productPageLoad() {
        if (sessionStorage.getItem('isCollapsed') === 'true') { //判斷選項收合
            collapse();
        }

        let realProducts = []; // not deleted products
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Product/list',  //獲取客戶資訊api
            type: 'GET',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    if (!response.data[i].isDelete) {
                        realProducts.push(response.data[i]);
                    }
                }

                global_products.pop();
                global_products.push(realProducts);
                //console.log(realProducts);

                document.getElementById('productlist').innerHTML = '';
                let len = (realProducts.length >= 10) ? 10 : realProducts.length;
                for (let i = 0; i < len; i++) {
                    document.getElementById('productlist').insertAdjacentHTML('beforeEnd', `            
                        <tr id="${realProducts[i].commodity_ID}">
                            <td>${realProducts[i].name}</td>
                            <td>${realProducts[i].price}</td>
                            <td>${realProducts[i].cost}</td> 
                            <td><i class="fas fa-info-circle" onclick="productInfo('${realProducts[i].commodity_ID}')"></i></td>
                            <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordC_id('${realProducts[i].commodity_ID}')"></i> <i class="fas fa-trash-alt" onclick="deleteProduct('${realProducts[i].name}','${realProducts[i].commodity_ID}') "></i></td>
                        </tr>                   
                    `);
                }

                if (realProducts.length % 10 == 0) {
                    page_num = realProducts.length / 10;
                } else {
                    page_num = parseInt(realProducts.length / 10) + 1;
                }

                pageEffect(page_num, 1);
            }
        });
    }
);

function productSearch() {
    let clue = document.getElementById("productSearch").value;
    let searchPageNum = 0;
    let realSearchProducts = [];
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Product/search?clue=' + clue,
        type: 'GET',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {

            if (response.data === null) {
                alert(response.message);
                return;
            } else {
                for (let i = 0; i < response.data.length; i++) {
                    {
                        if (!response.data[i].isDelete) {
                            realSearchProducts.push(response.data[i]);
                        }
                    }
                }
            }

            global_products.pop(); //清空全域顧客列表
            global_products.push(realSearchProducts);

            let len = 0;
            if (realSearchProducts.length < 10) {
                len = realSearchProducts.length;
            } else {
                len = 10;
            }

            document.getElementById('productlist').innerHTML = '';
            if (realSearchProducts.length > 0) {
                for (let i = 0; i < len; i++) {
                    document.getElementById('productlist').insertAdjacentHTML('beforeEnd', `            
                    <tr id="${realSearchProducts[i].commodity_ID}">
                        <td>${realSearchProducts[i].name}</td>
                        <td>${realSearchProducts[i].price}</td>
                        <td>${realSearchProducts[i].cost}</td> 
                        <td><i class="fas fa-info-circle" onclick="productInfo('${realSearchProducts[i].commodity_ID}')"></i></td>
                        <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordC_id('${realSearchProducts[i].commodity_ID}')"></i> <i class="fas fa-trash-alt" onclick="deleteProduct('${realSearchProducts[i].name}','${realSearchProducts[i].commodity_ID}') "></i></td>
                    </tr>                   
                    `);
                }
                if (realSearchProducts.length % 10 == 0) {
                    searchPageNum = realSearchProducts.length / 10;
                } else {
                    searchPageNum = parseInt((realSearchProducts.length / 10) + 1);
                }
                pageEffect(searchPageNum, 1);
            } else {
                alert('查無此產品！');
                location.reload();
            }
        }
    });
}

function addProduct() {
    const addname = document.getElementById("addproductname");
    const addprice = document.getElementById("addprice");
    const addcost = document.getElementById("addcost");
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Product/add',  //獲取客戶資訊api
        type: 'POST',
        data: {
            name: addname.value,
            price: addprice.value,
            cost: addcost.value
        },
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            addname.value = '';
            addprice.value = '';
            addcost.value = '';
            location.reload();
        }
    });
}

function deleteProduct(name, c_id) {
    let productTrTag = document.getElementById(c_id);
    let text = c_id;
    isRemove = confirm("確定移除 '" + name + "' ？");
    if (isRemove) {
        productTrTag.remove();
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Product/delete?commodity_ID=' + text,
            type: 'PUT',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                alert(response.message);
                location.reload();
            }
        });
    }
}

var tempc_id = '';
function recordC_id(c_id) {
    tempc_id = c_id;
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Product/search?clue=' + tempc_id,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            document.getElementById("editproductname").value = response.data[0].name;
            document.getElementById("editproductprice").value = response.data[0].price;
            document.getElementById("editproductcost").value = response.data[0].cost;
        }
    });
}

function editProduct() {
    const productName = document.getElementById("editproductname").value;
    const editprice = document.getElementById("editproductprice").value;
    const editcost = document.getElementById("editproductcost").value;

    let productInfo = {
        commodity_ID: tempc_id,
        price: editprice,
        cost: editcost,
        name: productName
    };

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Product/edit',
        type: 'POST',
        data: productInfo,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });
}

function pageEffect(totalpages, presentpage) { //頁碼按鈕
    //console.log(presentpage);
    //console.log(totalpages);

    const productPageul = document.querySelector('#productPageul');
    let productPageli = '';
    let beforePages = presentpage - 1;
    let afterPages = presentpage + 1;
    let activeli;
    if (presentpage > 1) {
        productPageli += `<li class="prev btn" onclick="pageEffect(${totalpages}, ${presentpage - 1}); changePage(${presentpage - 1}) "><span><i class="fas fa-chevron-left"></i>Prev</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage > 2) { //1 2 (3) 4
            productPageli += `<li class="num btn" onclick="pageEffect(${totalpages}, 1); changePage(1) "><span>1</span></li>`;
            if (presentpage > 3) {
                productPageli += `<li class="dots"><span>...</span></li>`;
            }
        }
    }

    if (presentpage == totalpages - 1) {
        beforePages = beforePages - 1;
    } else if (presentpage == totalpages) {
        beforePages = beforePages - 2;
    }

    if (presentpage == 2) {
        afterPages = afterPages + 1;
    } else if (presentpage == 1) {
        afterPages = afterPages + 2;
    }


    for (let i = beforePages; i <= afterPages; i++) { //以presentpage為中心
        if (i > totalpages) {
            break;
        }
        if (i < 1) {
            continue;
        }
        if (presentpage == i) {
            activeli = "active-page";
        } else {
            activeli = "";
        }
        productPageli += `<li class="num btn ${activeli}" onclick="pageEffect(${totalpages}, ${i}); changePage(${i}) "><span>${i}</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage < totalpages - 1) { //17 (18) 19 20
            if (presentpage < totalpages - 2) {
                productPageli += `<li class="dots"><span>...</span></li>`;
            }
            productPageli += `<li class="num btn"  onclick="pageEffect(${totalpages}, ${totalpages}); changePage(${totalpages}) "><span>${totalpages}</span></li>`;
        }
    }

    if (presentpage < totalpages) {
        productPageli += `<li class="next btn" onclick="pageEffect(${totalpages}, ${presentpage + 1}); changePage(${presentpage + 1})"><span>Next<i class="fas fa-chevron-right"></i></span></li>`;
    }
    productPageul.innerHTML = productPageli;
}

function changePage(targetpage) {
    let firstDataIndex = (targetpage - 1) * 10 + 1 - 1;
    let trtdTag = '';

    const tbodyTag = document.getElementById("productlist");

    tbodyTag.innerHTML = '';
    for (let i = firstDataIndex; i < firstDataIndex + 10; i++) {
        if (i < global_products[0].length) {
            trtdTag += `<tr id="${global_products[0][i].commodity_ID}">
                            <td>${global_products[0][i].name}</td>
                            <td>${global_products[0][i].price}</td>
                            <td>${global_products[0][i].cost}</td>
                            <td><i class="fas fa-info-circle" onclick="productInfo('${global_products[0][i].commodity_ID}')"></i></td>
                            <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordC_id('${global_products[0][i].commodity_ID}')" ></i><i class="fas fa-trash-alt" onclick="deleteProduct('${global_products[0][i].name}', '${global_products[0][i].commodity_ID}')"></i></td>
                        </tr>`;
        }
    }
    tbodyTag.innerHTML = trtdTag;
    //console.log(global_customers[0][1].name);
}

var previousHTML = "";
var previousStyle;
function productInfo(product_id) {
    let product = null;
    let imgs = [];
    let imgspath = [];
    let imgsbase64 = [];
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Product/search?clue=' + product_id,
        type: 'GET',
        async: false, //****先得到product 在執行下面
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            product = response.data[0];
        }
    });

    $.ajax({
        url: `https://makatabar.com/BandBManagement/api/Product/${product_id}/getImageBase64`,
        type: 'GET',
        async: false, //****先得到imgs
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            if (response.data != null) {
                imgs = response.data; //dictionary
            }
        }
    });

    for (let key in imgs) {
        imgspath.push(key);
        imgsbase64.push(imgs[key]);
    }

    let buffercontent = '';
    const pagecontent = document.getElementById('pagecontent');
    previousHTML = pagecontent.innerHTML;
    previousStyle = pagecontent.style;
    pagecontent.innerHTML = ''; //清空內部元素
    buffercontent += `
        <div class="back" onclick="goBack()"><i class="fas fa-chevron-left"></i>單一商品</div>
        <div class="input-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="productimg" onchange="chooseFile()" multiple>
                <label class="custom-file-label" for="productimg" id="productimglabel"><span>選擇檔案</span></label>
            </div>
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" onclick="uploadImg('${product_id}')">上傳</button>               
            </div>
        </div>       
        
    `;

    if (imgsbase64.length > 0) {
        buffercontent += `
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                
                <ol id="carouselol" class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                </ol>
                
                <div id="carouselcontent" class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="data:image0/jpg;base64,${imgsbase64[0]}" alt="0 slide">
                    </div>
                </div>
            
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        `;
    }
    buffercontent += `
        <div id="product_inner_info" style="display:inline-block; position:absolute">
            <ul>
                <li>房間名稱:&nbsp;&nbsp;${product.name}</li>
                <li>價格:&nbsp;&nbsp;${product.price}&nbsp;元</li>
                <li>成本:&nbsp;&nbsp;${product.cost}&nbsp;元</li>              
            </ul>
            
            <button id="checkimg" class="btn btn-outline-secondary" type="button" data-toggle="modal" data-target="#openimg">查看相片</button>
        </div>
    `;

    buffercontent += `
        <div class="modal" id="openimg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
        aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">
                                Close</span></button>
                        <h5 class="modal-title">查看相片</h5>
                    </div>
                    <div id="check_inner_photo_body" class="modal-body">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                            <label class="form-check-label" for="inlineCheckbox1">1</label>
                            <img class="checkinnerphoto" src="data:image0/jpg;base64,${imgsbase64[0]}"> 
                        </div>                      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            關閉</button>
                        <button id="delete_checked_imgs" type="button" class="btn btn-primary">刪除</button>
                    </div>
                </div>
            </div>
        </div>    
    `;

    pagecontent.innerHTML = buffercontent;
    for (let i = 1; i < imgsbase64.length; i++) { //先有html才能insert
        document.getElementById("check_inner_photo_body").insertAdjacentHTML("beforeend", `
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox${i + 1}" value="option${i + 1}">
                <label class="form-check-label" for="inlineCheckbox${i + 1}">${i + 1}</label>
                <img class="checkinnerphoto" src="data:image${i}/jpg;base64,${imgsbase64[i]}"> 
            </div>   
        `);
    }
    document.getElementById("delete_checked_imgs").onclick = function () { deleteProductImage(imgspath, product_id) };
    //這樣onclick參數才能傳遞array

    if (imgsbase64.length > 0) {
        for (let i = 1; i < imgsbase64.length; i++) {
            document.getElementById("carouselol").insertAdjacentHTML("beforeend", `
                 <li data-target="#carouselExampleIndicators" data-slide-to="${i}"></li>
            `);

            document.getElementById("carouselcontent").insertAdjacentHTML("beforeend", `
                <div class="carousel-item">
                    <img class="d-block w-100" src="data:image${i}/jpg;base64,${imgsbase64[i]}" alt="${i} slide">
                </div>
            `);
        }
        $('.carousel').carousel({
            interval: 2000
        });
    }
}

function deleteProductImage(imgspath, product_id) {
    let will_delete = [];
    let checks = document.getElementsByClassName("form-check-input");
    for (let i = 0; i < checks.length; i++) {
        if (checks[i].checked) {
            //console.log(imgspath[i]);
            will_delete.push(imgspath[i]);
        }
    }
    //console.log(will_delete);
    let product_image = {
        product_ID: product_id,
        will_delete_images: will_delete
    };
    // location.reload();

    $.ajax({
        url: `https://makatabar.com/BandBManagement/api/Product/deleteImage`,
        type: 'POST',
        async: false,
        data: product_image,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });

}

function chooseFile() {
    const files = document.getElementById('productimg').files;
    //console.log(files);
    let label = document.getElementById("productimglabel");
    label.innerHTML = '';
    for (let i = 0; i < files.length; i++) {
        label.innerHTML += `<span>${files[i].name}&nbsp;&nbsp;&nbsp;&nbsp;</span>`;
    }//&nbsp; 空格

}

function uploadImg(product_id) {
    const files = document.getElementById('productimg').files;
    let form = new FormData(); //FormData 與json同 有key,value 但value可放File

    for (var i = 0; i < files.length; i++) {
        form.append("files" + i, files[i]);
    }

    $.ajax({
        url: `https://makatabar.com/BandBManagement/api/Product/${product_id}/uploadFiles`,
        type: 'POST',
        data: form, //postman中 data欄位 有formData可填
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });
}