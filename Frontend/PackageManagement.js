var global_packages = [];

$(document).ready(
    function packagePageLoad() {
        if (sessionStorage.getItem('isCollapsed') === 'true') { //判斷選項收合
            collapse();
        }

        let real_packages = [];
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Package/getPackageslist',  //獲取客戶資訊api
            type: 'GET',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    if (!response.data[i].isDelete) {
                        real_packages.push(response.data[i]);
                    }
                }
                global_packages.pop();
                global_packages.push(real_packages);

                document.getElementById('packagelist').innerHTML = '';
                let len = (real_packages.length >= 10) ? 10 : real_packages.length;
                for (let i = 0; i < len; i++) {
                    document.getElementById('packagelist').insertAdjacentHTML('beforeEnd', `            
                        <tr id="${real_packages[i].package_ID}">
                            <td>${real_packages[i].package_type}</td>
                            <td>${real_packages[i].price}</td>
                            <td><i class="fas fa-info-circle" onclick="packageInfo('${real_packages[i].package_ID}')"></i></td>
                            <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordP_id('${real_packages[i].package_ID}')"></i> <i class="fas fa-trash-alt" onclick="deletePackage('${real_packages[i].package_type}','${real_packages[i].package_ID}') "></i></td>
                        </tr>                   
                    `);
                }

                if (real_packages.length % 10 == 0) {
                    page_num = real_packages.length / 10;
                } else {
                    page_num = parseInt(real_packages.length / 10) + 1;
                }

                pageEffect(page_num, 1);
            }
        });
    }
);

function packageSearch() {
    let clue = document.getElementById("packageSearch").value;
    let searchPageNum = 0;
    let realSearchPackages = [];
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Package/search?clue=' + clue,
        type: 'GET',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {

            if (response.data === null) {
                alert(response.message);
                return;
            } else {
                for (let i = 0; i < response.data.length; i++) {
                    {
                        if (!response.data[i].isDelete) {
                            realSearchPackages.push(response.data[i]);
                        }
                    }
                }
            }

            global_packages.pop();
            global_packages.push(realSearchPackages);

            let len = 0;
            if (realSearchPackages.length < 10) {
                len = realSearchPackages.length;
            } else {
                len = 10;
            }

            document.getElementById('packagelist').innerHTML = '';
            if (realSearchPackages.length > 0) {
                for (let i = 0; i < len; i++) {
                    document.getElementById('packagelist').insertAdjacentHTML('beforeEnd', `            
                        <tr id="${realSearchPackages[i].package_ID}">
                            <td>${realSearchPackages[i].package_type}</td>
                            <td>${realSearchPackages[i].price}</td>                    
                            <td><i class="fas fa-info-circle" onclick="packageInfo('${realSearchPackages[i].package_ID}')"></i></td>
                            <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordP_id('${realSearchPackages[i].package_ID}')"></i> <i class="fas fa-trash-alt" onclick="deletePackage('${realSearchPackages[i].package_type}','${realSearchPackages[i].package_ID}') "></i></td>
                        </tr>                   
                        `);
                }
                if (realSearchPackages.length % 10 == 0) {
                    searchPageNum = realSearchPackages.length / 10;
                } else {
                    searchPageNum = parseInt((realSearchPackages.length / 10) + 1);
                }
                pageEffect(searchPageNum, 1);
            } else {
                alert('查無此產品！');
                location.reload();
            }
        }
    });
}

function addPackage() {
    const addname = document.getElementById("addpackagename");
    const addprice = document.getElementById("addprice");
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Package/add',
        type: 'POST',
        data: {
            package_type: addname.value,
            price: addprice.value,
        },
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            addname.value = '';
            addprice.value = '';
            location.reload();
        }
    });
}

function deletePackage(name, p_id) {
    let packageTrTag = document.getElementById(p_id);
    let text = p_id;
    isRemove = confirm("確定移除 '" + name + "' ？");
    if (isRemove) {
        packageTrTag.remove();
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Package/delete?package_ID=' + text,
            type: 'PUT',
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                alert(response.message);
                location.reload();
            }
        });
    }
}

var tempp_id = '';
function recordP_id(p_id) {
    tempp_id = p_id;
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Package/search?clue=' + tempp_id,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            document.getElementById("editpackagename").value = response.data[0].package_type;
            document.getElementById("editpackageprice").value = response.data[0].price;
        }
    });
}

function editPackage() {
    const packageName = document.getElementById("editpackagename").value;
    const editprice = document.getElementById("editpackageprice").value;

    let packageInfo = {
        package_ID: tempp_id,
        package_type: packageName,
        price: editprice,
    };

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Package/edit',
        type: 'POST',
        data: packageInfo,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });
}

var previousHTML = "";
var previousStyle;
function packageInfo(p_id) {
    num = 1;
    let buffercontent = '';
    let pagecontent = document.getElementById("pagecontent");
    let package = [];
    let products = [];
    want_add_product = []; //刷新

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Package/search?clue=' + p_id,
        type: 'GET',
        async: false,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            package.push(response.data[0]);
        }
    });

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Product/list',
        type: 'GET',
        async: false,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            for (let i = 0; i < response.data.length; i++) {
                if (!response.data[i].isDelete) {
                    products.push(response.data[i]);
                }
            }
        }
    });

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Package/getRelatedProduct?package_ID=' + p_id,
        type: 'GET',
        async: false,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            for (let i = 0; i < response.data.length; i++) {
                want_add_product.push(response.data[i].name);
            }
        }
    });

    previousHTML = pagecontent.innerHTML;
    previousStyle = pagecontent.style;
    pagecontent.innerHTML = '';
    buffercontent += `
        <div class="back" onclick="goBack()"><i class="fas fa-chevron-left"></i>套裝行程</div>
        <div id="pkgtitle">
            <h3>${package[0].package_type}</h3>
        </div>
        <div class="dropdown" style="margin-top: 2%;">
            <label for="#product_dropdown" style="color: #777;">請選擇欲組合商品</label>
            <button id="product_dropdown" class="btn btn-secondary dropdown-toggle" type="button"
                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                商品
            </button>
            <div id="product_dropdown_menu" class="dropdown-menu" aria-labelledby="dropdownMenuButton" data-size="10">

            </div>
        </div>
        <div id="plusicon"><i class="fas fa-plus"></i><span>加入商品</span></div>
        <div id="will_combine_product">

        </div>
        <div id="confirmproducts" onclick="confirmProducts('${p_id}')">
            確認    
            <i class="fas fa-check"></i>
        </div>   
    `;

    pagecontent.innerHTML = buffercontent;
    for (let i = 0; i < products.length; i++) {
        document.getElementById("product_dropdown_menu").insertAdjacentHTML("beforeend", `
            <a class="dropdown-item" href="#" onclick="clickProductOpion('${p_id}', '${products[i].name}')">${products[i].name}</a>
        `);
    }

    for (let i = 0; i < want_add_product.length; i++) {
        document.getElementById("will_combine_product").insertAdjacentHTML('beforeend', `
            <div class="will_add_product" id="add${want_add_product[i]}">
                <ul id="productlist">
                    <li class="first-li">${num}</li>
                    <li class="second-li">${want_add_product[i]}</li>
                    <li class="third-li"><i class="fas fa-trash-alt" onclick="deleteWantAddProduct('${p_id}', '${want_add_product[i]}')"></i></li>
                </ul>
            </div>
        `);
        num += 1;
    }

}

var select_product_name = "";
function clickProductOpion(p_id, product_name) {
    select_product_name = product_name;
    document.getElementById("product_dropdown").innerText = product_name;
    document.getElementById("plusicon").onclick = function () { plusIcon(p_id, product_name) }; //等有出現plusicon元素再覆蓋onclick事件
}

var num = 1;
var want_add_product = [];
function plusIcon(p_id, p_name) {
    if (want_add_product.includes(p_id, p_name)) {
        alert("已存在商品！");
    } else {
        document.getElementById("will_combine_product").insertAdjacentHTML('beforeend', `
            <div class="will_add_product" id="add${p_name}">
                <ul id="productlist">
                    <li class="first-li">${num}</li>
                    <li class="second-li">${p_name}</li>
                    <li class="third-li"><i class="fas fa-trash-alt" onclick="deleteWantAddProduct('${p_id}', '${p_name}')"></i></li>
                </ul>
            </div>
        `);
        num += 1;
        want_add_product.push(p_name);
    }
}

function confirmProducts(packageID) {
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Package/relatedPackageProducts',
        type: 'POST',
        async: false,
        data: {
            package_ID: packageID,
            names: want_add_product
        },
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            //console.log(response);
            alert(response.message);
        }
    });
}

function deleteWantAddProduct(p_id, p_name) {

    var yes = confirm("確定刪除?");
    if (yes) {
        document.getElementById("add" + p_name).remove();
        const index = want_add_product.indexOf(p_name);

        if (index > -1) { //exist
            want_add_product.splice(index, 1); //startindex,deletecount     
        }

        let firstlis = document.getElementsByClassName("first-li");
        for (let i = 0; i < want_add_product.length; i++) {
            firstlis[i].innerText = i + 1;
        }
        num = want_add_product.length + 1;

        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Package/deleteRelatedProduct',
            type: 'POST',
            async: false,
            data: {
                package_ID: p_id,
                names: [
                    p_name
                ]
            },
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                //console.log(response);
                alert(response.message);
            }
        });
    } else { }
}

function pageEffect(totalpages, presentpage) { //頁碼按鈕
    //console.log(presentpage);
    //console.log(totalpages);

    const packagePageul = document.querySelector('#packagePageul');
    let packagePageli = '';
    let beforePages = presentpage - 1;
    let afterPages = presentpage + 1;
    let activeli;
    if (presentpage > 1) {
        packagePageli += `<li class="prev btn" onclick="pageEffect(${totalpages}, ${presentpage - 1}); changePage(${presentpage - 1}) "><span><i class="fas fa-chevron-left"></i>Prev</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage > 2) { //1 2 (3) 4
            packagePageli += `<li class="num btn" onclick="pageEffect(${totalpages}, 1); changePage(1) "><span>1</span></li>`;
            if (presentpage > 3) {
                packagePageli += `<li class="dots"><span>...</span></li>`;
            }
        }
    }

    if (presentpage == totalpages - 1) {
        beforePages = beforePages - 1;
    } else if (presentpage == totalpages) {
        beforePages = beforePages - 2;
    }

    if (presentpage == 2) {
        afterPages = afterPages + 1;
    } else if (presentpage == 1) {
        afterPages = afterPages + 2;
    }


    for (let i = beforePages; i <= afterPages; i++) { //以presentpage為中心
        if (i > totalpages) {
            break;
        }
        if (i < 1) {
            continue;
        }
        if (presentpage == i) {
            activeli = "active-page";
        } else {
            activeli = "";
        }
        packagePageli += `<li class="num btn ${activeli}" onclick="pageEffect(${totalpages}, ${i}); changePage(${i}) "><span>${i}</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage < totalpages - 1) { //17 (18) 19 20
            if (presentpage < totalpages - 2) {
                packagePageli += `<li class="dots"><span>...</span></li>`;
            }
            packagePageli += `<li class="num btn"  onclick="pageEffect(${totalpages}, ${totalpages}); changePage(${totalpages}) "><span>${totalpages}</span></li>`;
        }
    }

    if (presentpage < totalpages) {
        packagePageli += `<li class="next btn" onclick="pageEffect(${totalpages}, ${presentpage + 1}); changePage(${presentpage + 1})"><span>Next<i class="fas fa-chevron-right"></i></span></li>`;
    }
    packagePageul.innerHTML = packagePageli;
}

function changePage(targetpage) {
    let firstDataIndex = (targetpage - 1) * 10 + 1 - 1;
    let trtdTag = '';

    const tbodyTag = document.getElementById("packagelist");

    tbodyTag.innerHTML = '';
    for (let i = firstDataIndex; i < firstDataIndex + 10; i++) {
        if (i < global_packages[0].length) {
            trtdTag += `<tr id="${global_packages[0][i].package_ID}">
                                <td>${global_packages[0][i].package_type}</td>
                                <td>${global_packages[0][i].price}</td>
                                <td><i class="fas fa-info-circle" onclick="packageInfo('${global_packages[0][i].package_ID}')"></i></td>
                                <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordP_id('${global_packages[0][i].package_ID}')" ></i><i class="fas fa-trash-alt" onclick="deletePackage('${global_packages[0][i].package_type}', '${global_packages[0][i].package_ID}')"></i></td>
                            </tr>`;
        }
    }
    tbodyTag.innerHTML = trtdTag;
}