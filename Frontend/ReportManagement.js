$(document).ready(
    function ReportPageLoad() {
        if (sessionStorage.getItem('isCollapsed') === 'true') {
            collapse();
        }
    }

);

var orders_data = [];
var analysis_data = [];
//查詢功能
function reportSearch() {
    let first = document.getElementById("firstdate");
    let last = document.getElementById("lastdate");

    let firstlist = first.value.split("-").map(x => parseInt(x)); //map 將原list 經由map 回傳一個新的list
    let lastlist = last.value.split("-").map(x => parseInt(x));

    if (isNaN(firstlist[0]) || isNaN(lastlist[0])) {
        alert("請選擇日期！");
        location.reload();
    } else {
        if (firstlist[0] <= lastlist[0]) //年分ok
        {
            if (firstlist[1] <= lastlist[1] || (firstlist[0] < lastlist[0])) //月份ok 或 年分OK
            {
                if ((firstlist[2] < lastlist[2]) || (firstlist[0] < lastlist[0]) || (firstlist[1] < lastlist[1])) //日ok 或 年分OK 或 月份ok
                {
                    let newfirst = first.value.changeDatePos();
                    let newlast = last.value.changeDatePos();

                    let str = `firstdate=${newfirst}&lastdate=${newlast}`;
                    $.ajax({
                        url: 'https://makatabar.com/BandBManagement/api/Report/getRangeReport?' + str,
                        type: 'POST',
                        async: false,
                        error: function (xhr) {
                            alert('Ajax request 發生錯誤');
                        },
                        success: function (response) {
                            orders_data = response.data;
                            // console.log(response);
                            //console.log(getSimpleDateList2(response.data));

                            if (response.code == 500) {
                                alert(response.message);
                                return;
                            }

                            let normal_report = document.getElementById("normal_report");
                            normal_report.innerHTML = '';

                            let sum = [0, 0, 0, 0, 0, 0];


                            for (let i = 0; i < response.data.length; i++) {
                                normal_report.insertAdjacentHTML('beforeend', `
                                    <tr>
                                        <!--<td>${response.data[i].createTime.showSimpleDate()}</td>-->
                                        <!--<td>${response.data[i].createTime.showDay()}</td>-->
                                        <td>${response.data[i].checkinDate == "" ? "" : response.data[i].checkinDate.split('-').join('/')}</td>
                                        <td>${response.data[i].checkinDate == "" ? "" : response.data[i].checkinDate.showDay2()}</td>
                                        <td>${response.data[i].deposit_date}</td>
                                        <td>${response.data[i].deposit}</td>
                                        <td>${response.data[i].people_num}</td>
                                        <td>${response.data[i].cash_income}</td>
                                        <td>${response.data[i].additional_purchase}</td>
                                        <td>${response.data[i].cash_income + response.data[i].additional_purchase}</td>
                                        <td>${response.data[i].total_money}</td>                                      
                                    </tr>
                                `);
                                sum[0] += response.data[i].deposit;
                                sum[1] += response.data[i].people_num
                                sum[2] += response.data[i].cash_income
                                sum[3] += response.data[i].additional_purchase
                                sum[4] += response.data[i].cash_income + response.data[i].additional_purchase
                                sum[5] += response.data[i].total_money
                            }
                            normal_report.insertAdjacentHTML('beforeend', `
                                <tr>
                                    <th colspan="3" style="text-align: center;">合計</th>
                                    <td>${sum[0]}</td>
                                    <td>${sum[1]}</td>
                                    <td>${sum[2]}</td>
                                    <td>${sum[3]}</td>
                                    <td>${sum[4]}</td>
                                    <td>${sum[5]}</td>
                                </tr>
                            `);
                        }
                    });
                } else {
                    alert("末日期不可小於或等於初日期！");
                    location.reload();
                }
            } else {
                alert("末月分不可小於初月分！");
                location.reload();
            }
        } else {
            alert("末年分不可小於初年分！");
            location.reload();
        }
    }
}

let analysisreport = {};
function analysisReportSearch() {
    let costofgoodtbody = document.querySelector("#costofgoodreport > tbody");
    let sellingcosttbody = document.querySelector("#sellingcostreport > tbody");
    let profitreport = document.querySelector("#profitreport > tbody");
    let shareholdertbody = document.querySelector("#shareholderreport > tbody");

    costofgoodtbody.innerHTML = '';
    sellingcosttbody.innerHTML = '';
    profitreport.innerHTML = '';
    shareholdertbody.innerHTML = '';

    //get year month
    if (document.getElementById("monthdate").value == "") {
        alert("請選擇日期!");
        return;
    }

    let year_month = document.getElementById("monthdate").value.split("-");
    let str = `year=${year_month[0]}&month=${year_month[1]}`;
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Report/getAnalysis?' + str,
        type: 'POST',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            analysis_data = response.data;

            let costofgood_data = response.data["cost_of_good"] === null ? [] : JSON.parse(response.data["cost_of_good"].jsondata);
            let sellingcost_data = response.data["selling_cost"] === null ? [] : JSON.parse(response.data["selling_cost"].jsondata);
            let profit_data = response.data["profit"] === null ? [] : JSON.parse(response.data["profit"].jsonprofit);
            let share_data = response.data["share"] === null ? [] : JSON.parse(response.data["share"].jsonshare);

            let record = {
                "銷貨成本": 0,
                "銷貨費用": 0,
                "利潤分析": 0,
                "股東分紅": 0
            };

            analysisreport = {
                "銷貨成本": costofgood_data,
                "銷貨費用": sellingcost_data,
                "利潤分析": profit_data,
                "股東分紅": share_data
            };

            if (costofgood_data.length != 0) //jsondata null
            {
                drawAnalysisTable(costofgood_data, costofgoodtbody);
                record["銷貨成本"] = 1;
            }
            if (sellingcost_data.length != 0) {
                drawAnalysisTable(sellingcost_data, sellingcosttbody);
                record["銷貨費用"] = 1;
            }
            if (profit_data.length != 0) {
                drawAnalysisTable(profit_data, profitreport, true);
                record["利潤分析"] = 1;

                if (share_data.length != 0) {
                    //drawAnalysisTable(share_data, shareholdertbody);
                    drawShareTable(share_data, shareholdertbody, profit_data[3]["value"])
                    record["股東分紅"] = 1;
                }
            }

            let str = "";
            for (let key in record) {
                if (record[key] == 0) {
                    str += key + " ";
                }
            }

            if (str != "") {
                alert(str + "報表尚未新增");
            }

            $(".collapse").collapse('show');
            $(".collapse").collapse();
        }
    });
}

function allShow() {
    $(".collapse").collapse('show');
    $(".collapse").collapse();
}
function allCollapse() {
    $(".collapse").collapse('hide');
}


//新增功能
var costtype = "";
var rownum = 0;
var count = 0;
var costList = [];
function openCostAdd() {
    rownum = 0;
    count = 0;
    let form = document.getElementById("add_cost_form");
    let items = document.getElementsByClassName("cost-item");
    let len = items.length;

    //items 會不斷刷新
    for (let i = len - 1; i >= 0; i--) //要倒回來 否則會抓不到index
    {
        let willremove = items[i]
        form.removeChild(willremove);
    }

    document.getElementById("typebtn").innerText = "報表類型";
    document.getElementById("adddate").value = "";
}
function recordCost(type, chtype) {
    costtype = type;
    document.getElementById("typebtn").innerText = chtype;
}
function confirmCost() {
    if (document.getElementById("adddate").value === "") {
        alert("請選擇日期！");
        return;
    }

    if (costtype == "") {
        alert("請選擇報表類型！");
        return;
    }

    costList = []; //必須先清空
    //get all input value to json
    let cost_money = 0;
    let costs = document.getElementsByClassName("cost-item");
    for (let i = 0; i < costs.length; i++) {
        //key
        let key = costs[i].children.item(1).firstElementChild.value;

        //value
        let value = costs[i].children.item(2).firstElementChild.value;

        if (key == "" || value == "") {
            alert("請輸入資料!");
            return;
        }

        cost_money += parseInt(value);

        costList.push(
            {
                key: key,
                value: parseInt(value)
            }
        );
    }

    if (costList.length == 0) {
        alert("請輸入成本！");
        return;
    }

    let year_month = document.getElementById("adddate").value.split("-");
    let jsonstr = JSON.stringify(costList);
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Report/addCost',
        type: 'POST',
        async: false,
        data: {
            type: costtype,
            year: year_month[0],
            month: year_month[1],
            jsondata: jsonstr,
            cost_money: cost_money
        },
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);

            // console.log(response.data);
            // console.log(JSON.parse(response.data["jsondata"]));
            //console.log(JSON.parse(response.data)["營業額"]);
        }
    });

}
function addCostRow() {
    rownum++;
    count++;
    let form = document.getElementById("add_cost_form");
    form.insertAdjacentHTML('beforeend', `
        <div id="cost-${count}" class="cost-item">
            <div class="first-part"><h4>${rownum}</h4></div>
            <div class="second-part">
                <input type="text" class="form-control" placeholder="成本名稱">
            </div>
            <div class="third-part">
                <input type="number" min="0" class="form-control"
                    placeholder="成本費用">
            </div>
            <div class="fourth-part"><i class="fas fa-trash-alt" onclick="deleteCostItem('cost-${count}') "></i></div>
        </div>
    `);
}
function deleteCostItem(costid) {
    rownum--;
    document.getElementById("add_cost_form").removeChild(document.getElementById(costid));
    let firsts = document.getElementsByClassName("first-part");
    for (let i = 0; i < rownum; i++) {
        firsts[i].firstChild.innerText = i + 1;
    }
}

let shareholder = [];
function openShare() {
    rownum = 0;
    count = 0;
    let form = document.getElementById("add_share_form");
    let items = document.getElementsByClassName("share-item");
    let len = items.length;


    for (let i = len - 1; i >= 0; i--) {
        let willremove = items[i]
        form.removeChild(willremove);
    }
    document.getElementById("addsharedate").value = "";
}
function addShareRow() {
    rownum++;
    count++;
    let form = document.getElementById("add_share_form");
    form.insertAdjacentHTML('beforeend', `
        <div id="share-${count}" class="share-item">
            <div class="first-part"><h4>${rownum}</h4></div>
            <div class="second-part">
                <input type="text" class="form-control" placeholder="股東名稱">
            </div>
            <div class="third-part">
                <input type="number" min="0" class="form-control"
                    placeholder="分配百分比數">
            </div>
            <div class="fourth-part"><i class="fas fa-trash-alt" onclick="deleteShareItem('share-${count}') "></i></div>
        </div>
    `);
}
function deleteShareItem(shareid) {
    rownum--;
    document.getElementById("add_share_form").removeChild(document.getElementById(shareid));
    let firsts = document.getElementsByClassName("first-part");
    for (let i = 0; i < rownum; i++) {
        firsts[i].firstChild.innerText = i + 1;
    }
}
function confirmShare() {
    shareholder = [];
    let checksum = 0;
    let shares = document.getElementsByClassName("share-item");
    for (let i = 0; i < shares.length; i++) {
        let key = shares[i].children.item(1).firstElementChild.value;

        let value = shares[i].children.item(2).firstElementChild.value;
        if (key == "" || value == "") {
            alert("請輸入資料!");
            return;
        }

        value = parseFloat(shares[i].children.item(2).firstElementChild.value);
        checksum += value;

        shareholder.push(
            {
                key: key,
                value: value
            }
        );
    }

    if (checksum != 100) {
        alert("百分比總和必須為100!");
        return;
    }

    if (shareholder.length == 0) {
        alert("請輸入股東分紅!");
        return;
    }

    let year_month = document.getElementById("addsharedate").value.split("-");
    let jsonstr = JSON.stringify(shareholder);
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Report/addShareHolder',
        type: 'POST',
        async: false,
        data: {
            year: year_month[0],
            month: year_month[1],
            jsonshare: jsonstr,
        },
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
        }
    });
}

//編輯功能
function editAnalysis(tableid) {
    let date = document.querySelector("#edit_form > input");
    date.value = document.getElementById("monthdate").value;
    if (date.value == "") {
        alert("請先查詢!");
        $("#edit").modal('show'); //手動打開 因為會先開一次
        return;
    }

    let type = document.querySelector(`#${tableid} > thead > tr > th`).innerText;
    if (analysisreport[type].length == 0) {
        alert("請先新增!");
        $("#edit").modal('show');
        return;
    }

    document.querySelector("#edit_form > div > button").innerText = type;

    rownum = 0;
    count = 0;
    let form = document.getElementById("edit_form");
    let items = document.getElementsByClassName("edit-item");
    let len = items.length;

    for (let i = len - 1; i >= 0; i--) {
        let willremove = items[i]
        form.removeChild(willremove);
    }
    document.getElementById("addsharedate").value = "";

    let data = analysisreport[type];
    for (let i = 0; i < data.length; i++) {
        count++;
        rownum++;
        form.insertAdjacentHTML('beforeend', `
            <div id="edit-${count}" class="edit-item">
                <div class="first-part"><h4>${rownum}</h4></div>
                <div class="second-part">
                    <input type="text" class="form-control" placeholder="名稱" value='${data[i].key}'>
                </div>
                <div class="third-part">
                    <input type="number" min="0" class="form-control"
                        placeholder="數值" value=${data[i].value}>
                </div>
                <div class="fourth-part"><i class="fas fa-trash-alt" onclick="deleteEditItem('edit-${count}') "></i></div>
            </div>
        `);
    }
}
function addEditRow() {
    rownum++;
    count++;
    let form = document.getElementById("edit_form");
    form.insertAdjacentHTML('beforeend', `
        <div id="edit-${count}" class="edit-item">
            <div class="first-part"><h4>${rownum}</h4></div>
            <div class="second-part">
                <input type="text" class="form-control" placeholder="名稱">
            </div>
            <div class="third-part">
                <input type="number" min="0" class="form-control"
                    placeholder="數值">
            </div>
            <div class="fourth-part"><i class="fas fa-trash-alt" onclick="deleteEditItem('edit-${count}') "></i></div>
        </div>
    `);
}
function deleteEditItem(editid) {
    rownum--;
    document.getElementById("edit_form").removeChild(document.getElementById(editid));
    let firsts = document.getElementsByClassName("first-part");
    for (let i = 0; i < rownum; i++) {
        firsts[i].firstChild.innerText = i + 1;
    }
}
function editReport() {
    let type = document.querySelector("#edit_form > div > button").innerText;
    let items = document.getElementsByClassName("edit-item");
    let date = document.querySelector("#edit_form > input").value.split("-");

    let sum = 0;
    if (type === '股東分紅') {
        for (let i = 0; i < items.length; i++) {
            sum += parseInt(items[i].children.item(2).firstElementChild.value);
        }
        if (sum != 100) {
            alert("百分比總和必須為100!");
            return;
        }
    }

    //利用analysisreport 一次性編輯 順便編輯 利潤分析
    let temp = [];
    let tempsum = 0;
    for (let i = 0; i < items.length; i++) {
        let key = items[i].children.item(1).firstElementChild.value;
        let value = items[i].children.item(2).firstElementChild.value;

        if (key == "" || value == "") {
            alert("請輸入資料!");
            return;
        }

        tempsum += parseInt(value);

        temp.push({
            key: key,
            value: parseInt(value)
        });
    }
    if (temp.length == 0) {
        alert("請輸入資料!");
        return;
    }

    //判斷有無利潤分析 並發送ajax
    //有利潤分析 需先計算好
    let data = {};
    analysisreport[type] = temp;
    if (type != "股東分紅") //CostReport
    {
        //利潤分析
        if (analysisreport["利潤分析"].length > 0 && (type == "銷貨成本" || type == "銷貨費用")) {
            //改淨利、成本或費用
            let original_cost = 0;
            let original_turnover = 0;
            analysisreport["利潤分析"].map(x => {
                if (x.key == "營業額") {
                    original_turnover = x.value;
                } else {
                    if (x.key != "營運淨利") {
                        original_turnover -= x.value;
                    }
                }

                if ((x.key == "銷貨成本" && type == "銷貨成本") || (x.key == "銷貨費用" && type == "銷貨費用")) {
                    original_cost = x.value;
                    x.value = tempsum;
                } else if (x.key == "營運淨利") {
                    //let original_value = x.value;
                    //x.value = original_value - (tempsum - original_cost);
                    x.value = original_turnover;
                }
                return x;
            });
        }
    }

    data = {
        costofgooddata: JSON.stringify(analysisreport["銷貨成本"]),
        sellingcostdata: JSON.stringify(analysisreport["銷貨費用"]),
        profitdata: JSON.stringify(analysisreport["利潤分析"]),
        sharedata: JSON.stringify(analysisreport["股東分紅"]),
        edittype: type,
        year: date[0],
        month: date[1],
        costtotal: tempsum //股東分紅不影響 後端有處理
    };
    //console.log(data);
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Report/editAnalysisReport',
        type: 'POST',
        async: false,
        data: data,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });

}

function saveExcel() {
    const first = document.getElementById("firstdate").value === "" ? "novalue" : document.getElementById("firstdate").value;
    const last = document.getElementById("lastdate").value === "" ? "novalue" : document.getElementById("lastdate").value;
    const year = document.getElementById("monthdate").value.split("-")[0] === "" ? "novalue" : document.getElementById("monthdate").value.split("-")[0];
    const month = typeof document.getElementById("monthdate").value.split("-")[1] === "undefined" ? "novalue" : document.getElementById("monthdate").value.split("-")[1];
    //會超出索引

    //console.log(first, last, year, month);

    if (first == "novalue" || last == "novalue") {
        alert("請填入訂單報表日期範圍!");
        return;
    }

    const param = `first=${first}&last=${last}&year=${year}&month=${month}`;
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Report/exportExcel?' + param,
        type: 'POST',
        async: false,
        success: function (response) {
            //.done() 類似於 ajax success
            //這邊一定要用原生的 document.createElement
            //jQuery 沒辦法真的模擬原生的 click event
            //$ --> 註記HTML標籤物件
            const $a = document.createElement("a");
            // 給下載回來的資料產生一個網址
            const url = response.data.base64toURL();
            // 設定下載的檔名
            let date = Date.now();
            $a.download = `${date.toString()}_Report.xlsx`;
            // 設定網址
            $a.href = url;
            // 模擬使用者按下連結
            $a.click();
            alert(response.message);
            // 5 秒後清除產生的網址，這時候使用者應該按下了下載的按鈕了
            // 不清除的話會造成記憶體的浪費，這不會中斷使用者的下載
            // 不過若你下載完就要跳轉到下一頁的話，其實這個可以不用
            setTimeout(() => URL.revokeObjectURL(url), 5000)
        },
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        }
    });
}

function drawAnalysisTable(data, tbody, isprofit = false) {
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
        tbody.insertAdjacentHTML('beforeend', `
            <tr>
                <th>${data[i].key}</th>
                <td>${data[i].value}</td>
            </tr>                              
        `);
        sum += data[i].value;
    }

    if (!isprofit) {
        tbody.insertAdjacentHTML('beforeend', `
            <tr>
                <th style="text-align: right;">合計</th>
                <td>${sum}</td>
            </tr>
        `);
    }
}
function drawShareTable(data, tbody, total) {
    let sum = 0;
    for (let i = 0; i < data.length; i++) {
        tbody.insertAdjacentHTML('beforeend', `
            <tr>
                <th>${data[i].key}</th>
                <td>${data[i].value + "%"}</td>
                <td>${((data[i].value / 100) * total).toFixed(3)}</td>
            </tr> 
        `);
        sum += Number(((data[i].value / 100) * total).toFixed(3));
    }

    tbody.insertAdjacentHTML('beforeend', `
        <tr>
            <th colspan=2 style="text-align: right;">合計</th>
            <td>${sum}</td>
        </tr>
    `);
}

var previousHTML = '';
var previousStyle;
function drawChart() {
    if (orders_data.length == 0) {
        alert("請先查詢訂單!");
        return;
    }
    if (analysis_data.length == 0) {
        alert("請先查詢分析報表!");
        return;
    }

    let page = document.getElementById("pagecontent");
    previousHTML = page.innerHTML;
    previousStyle = page.style;
    page.style.backgroundImage = 'linear-gradient(rgb(28 38 49),rgb(28 38 49))';     /*顏色漸變函數  角度 顏色*/
    page.style.backgroundSize = '100% 100%';

    page.innerHTML = '';

    //使用Echart  
    //https://echarts.apache.org/zh/tutorial.html#ECharts%20%E5%9F%BA%E7%A1%80%E6%A6%82%E5%BF%B5%E6%A6%82%E8%A7%88
    //https://blog.csdn.net/weixin_43294560/category_9818009.html
    //https://echarts.apache.org/examples/zh/index.html
    /*
        訂單統計圖表: (用一筆一筆訂單座分隔 x軸為日期)
            1.營業額 --> 曲線圖
            2.人數 --> 柱狀圖
        分析統計圖表: 
            1. 成本皆用南丁格爾圖
            2. 利潤分析 --> 獨立選取時間 多個曲線圖
            3. 股東分紅 --> 環狀圖
    */


    page.insertAdjacentHTML('beforeend', `
        <div class="back-report" onclick="goBack()"><i class="fas fa-chevron-left"></i>報表</div>
        <div class="charts">
            <div class="chart-card" id="chartmain" style="width:700px; height: 600px;"></div>
            <div class="chart-card" id="chartmain-2" style="width:700px; height: 600px;"></div>
        </div>  
        <div class="charts">
            <div class="chart-card" id="chartmain-3" style="width:500px; height: 700px;"></div>
            <div class="chart-card" id="chartmain-4" style="width:500px; height: 700px;"></div>
            <div class="chart-card" id="chartmain-5" style="width:500px; height: 700px;"></div>
        </div>
        <div id="value-group" style="margin: 1%">
            <div><span style="font-size: medium; color: #fff">利潤分析日期</span></div>
            <input id="profitFirst" type="month" class="reportdate">
            <span style="font-size: x-large; color: #fff">~</span>
            <input id="profitLast" type="month" class="reportdate">
            <div class="normal-button" style="margin-left: 5%" onclick="searchRangeProfit()">
                <i class="fas fa-search"></i>
                查詢
            </div>
        </div>
        <div class="charts"> 
            <div class="chart-card" id="chartmain-6" style="width: 1500px; height: 700px;"></div>
        </div>
    `);

    let option = {
        title: {
            text: '訂單營業額',
            textStyle: {
                color: '#fff'
            }
        },
        textStyle: {
            color: '#fff'
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: { show: true }
            }
        },
        tooltip: {},

        legend: {
            orient: 'vertical',
            x: 'center',
            y: '0',
            padding: 10,
            itemGap: 20,
            textStyle: { color: '#c3cad9' }
        },

        xAxis: {
            type: 'category',
            data: getSimpleDateList2(orders_data)
        },
        yAxis: {},
        dataZoom: [
            {   //默認控制xAxis
                type: 'slider', //slider 型
                start: 0,      // 左邊在 10% 的位置。
                end: 100         // 右邊在 60% 的位置。
            }
        ],
        series: [{
            name: "單日營業額",
            type: 'line',
            data: getOrderBusinessValue(orders_data),
            itemStyle: {
                normal: {
                    // lineStyle: {
                    //     color: '#4ada92'
                    // },
                    color: '#4ada92'

                }
            },
            markPoint: {
                data: [
                    { type: 'max', name: '最大值' },
                    { type: 'min', name: '最小值' }
                ]
            },
            markLine: {
                data: [
                    { type: 'average', name: '平均值' }
                ]
            }
        }]
    };

    let option2 = {
        title: {
            text: '訂單人數',
            textStyle: {
                color: '#fff'
            }
        },
        textStyle: {
            color: '#fff'
        },
        tooltip: {},
        toolbox: {
            show: true,
            feature: {
                saveAsImage: { show: true }
            }
        },
        legend: {
            orient: 'vertical',
            x: 'center',
            y: '0',
            padding: 10,
            itemGap: 20,
            textStyle: { color: '#c3cad9' }
        },
        xAxis: {
            type: 'category',
            data: getSimpleDateList2(orders_data)
        },
        yAxis: {},
        dataZoom: [
            {   //默認控制xAxis
                type: 'slider', //slider 型
                start: 0,      // 左邊在 10% 的位置。
                end: 100         // 右邊在 60% 的位置。
            }
        ],
        series: [{
            name: "單日顧客人數",
            type: 'bar',
            data: getPeopleNum(orders_data),
            itemStyle: {
                normal: {
                    color: '#4ada92'
                }
            },
            markPoint: {
                data: [
                    { type: 'max', name: '最大值' },
                    { type: 'min', name: '最小值' }
                ]
            },
            markLine: {
                data: [
                    { type: 'average', name: '平均值' }
                ]
            },
        }]
    };

    let option3 = {
        title: {
            text: '銷貨成本 (' + analysis_data["cost_of_good"].year + '年' + analysis_data["cost_of_good"].month + '月)',
            textStyle: {
                color: '#fff'
            }
        },
        textStyle: {
            color: '#fff'
        },
        legend: {
            orient: 'horizontal',
            x: 'left',
            y: '90%',
            padding: 10,
            itemGap: 20,
            textStyle: { color: '#c3cad9' }
        },
        tooltip: {
            trigger: 'item',
            formatter: function (param) {
                let sum = largenData(getAnalysisParse(analysis_data["cost_of_good"].jsondata)).original_sum;
                let additional = largenData(getAnalysisParse(analysis_data["cost_of_good"].jsondata)).additional;

                return param.seriesName + '<br/>' + param.name + ': ' + (param.value - additional) + ' (' + (((param.value - additional) / sum) * 100).toFixed(2) + '%)'
            },
            //formatter: "{a} <br/>{b} : {c} ({d}%)"
            /* 
                {a}：系列名。
                {b}：数据名。
                {c}：数据值。
                {d}：百分比
            */
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: { show: true },
                dataView: { show: true, readOnly: true }
            },
        },
        calculable: true,
        series: [
            {
                name: '銷貨成本',
                type: 'pie',
                radius: '70%',  //內半徑, 外半徑
                center: ['50%', '50%'],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            fontSize: '15',
                            borderWidth: 1,
                            textStyle: {
                                color: '#fff'
                            }
                        },
                        labelLine: {
                            show: true
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        }
                    }
                },
                roseType: 'radius',
                // selectedMode: 'single',
                // selectedOffset: 10,
                data: largenData(getAnalysisParse(analysis_data["cost_of_good"].jsondata)).largen_data
            }
        ]
    };

    let option4 = {
        title: {
            text: '銷貨費用 (' + analysis_data["selling_cost"].year + '年' + analysis_data["selling_cost"].month + '月)',
            textStyle: {
                color: '#fff'
            }
        },
        textStyle: {
            color: '#fff'
        },
        legend: {
            orient: 'horizontal',
            x: 'left',
            y: '4%',
            padding: 10,
            itemGap: 20,
            textStyle: { color: '#c3cad9' }
        },
        tooltip: {
            trigger: 'item',
            formatter: function (param) {
                let sum = largenData(getAnalysisParse(analysis_data["selling_cost"].jsondata)).original_sum;
                let additional = largenData(getAnalysisParse(analysis_data["selling_cost"].jsondata)).additional;

                return param.seriesName + '<br/>' + param.name + ': ' + (param.value - additional) + ' (' + (((param.value - additional) / sum) * 100).toFixed(2) + '%)'
            },
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: { show: true },
                dataView: { show: true, readOnly: true }
            }
        },
        calculable: true,
        series: [
            {
                name: '銷貨費用',
                type: 'pie',
                radius: '70%',  //內半徑, 外半徑
                center: ['50%', '55%'],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            fontSize: '15',
                            borderWidth: 1,
                            textStyle: {
                                color: '#fff'
                            }
                        },
                        labelLine: {
                            show: true
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        }
                    }
                },
                roseType: 'radius',
                // selectedMode: 'single',
                // selectedOffset: 10,
                data: largenData(getAnalysisParse(analysis_data["selling_cost"].jsondata)).largen_data
            }
        ]
    };

    let option5 = {
        title: {
            text: '股東分紅 (' + analysis_data["share"].year + '年' + analysis_data["share"].month + '月)',
            textStyle: {
                color: '#fff'
            }
        },
        textStyle: {
            color: '#fff'
        },
        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },
        legend: {
            orient: 'horizontal',
            x: 'left',
            y: '90%',
            padding: 10,
            itemGap: 20,
            textStyle: { color: '#c3cad9' }
        },
        toolbox: {
            show: true,
            feature: {
                saveAsImage: { show: true },
                dataView: { show: true, readOnly: true }
            }
        },
        calculable: true,
        series: [
            {
                name: '銷貨費用',
                type: 'pie',
                radius: ['40%', '70%'],  //內半徑, 外半徑
                center: ['50%', '50%'],
                itemStyle: {
                    normal: {
                        label: {
                            show: true,
                            // position: 'inside',
                            fontSize: '15',
                            borderWidth: 1,
                            textStyle: {
                                color: '#fff'
                            }
                        },
                        labelLine: {
                            show: true
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        }
                    }
                },
                data: getAnalysisParse(analysis_data["share"].jsonshare, true),

            }
        ]
    };



    //初始化
    let mychart = echarts.init(document.getElementById("chartmain"));
    let mychart2 = echarts.init(document.getElementById("chartmain-2"));
    let mychart3 = echarts.init(document.getElementById("chartmain-3"));
    let mychart4 = echarts.init(document.getElementById("chartmain-4"));
    let mychart5 = echarts.init(document.getElementById("chartmain-5"));

    setTimeout(function () {
        document.getElementById("chartmain").style.display = 'inline-block';
        document.getElementById("chartmain-2").style.display = 'inline-block';
        document.getElementById("chartmain-3").style.display = 'inline-block';
        document.getElementById("chartmain-4").style.display = 'inline-block';
        document.getElementById("chartmain-5").style.display = 'inline-block';
        document.getElementById("chartmain-6").style.display = 'inline-block';
        document.getElementById("value-group").style.display = 'block';
        mychart.setOption(option);
        mychart2.setOption(option2);
        mychart3.setOption(option3);
        mychart4.setOption(option4);
        mychart5.setOption(option5);

    }, 1000);



}

function getSimpleDateList(data) {
    let dates = [];
    data.forEach(function (item) {
        dates.push(item.createTime.showSimpleDate());
    });
    dates = dates.reverse();

    return dates;
}
function getSimpleDateList2(data) {
    let dates = [];
    data.forEach(function (item) {
        dates.push(item.checkinDate == "" ? "-" : item.checkinDate.split('-').join('/'));
    });
    dates = dates.reverse();

    return dates;
}
function getOrderBusinessValue(data) {
    let values = [];
    data.forEach(function (item) {
        values.push(item.total_money);
    });
    values = values.reverse();

    return values;
}
function getPeopleNum(data) {
    let people = [];
    data.forEach(function (item) {
        people.push(item.people_num);
    });
    people = people.reverse();

    return people;
}
function getAnalysisParse(data, isShare = false) {
    let original = JSON.parse(data);
    let parsed = [];
    original.forEach(function (item) {
        value = isShare ? ((item["value"] / 100) * (JSON.parse(analysis_data["profit"].jsonprofit).filter(x => x.key == "營運淨利")[0]["value"])).toFixed(3) : item["value"];
        parsed.push({ name: item["key"], value: value });
    });

    return parsed;
}
function largenData(data) {
    let sum = 0;
    let max = data[0].value;

    data.forEach(x => {
        sum += x.value;
        max = x.value >= max ? x.value : max;
    });
    let additional = Math.round(max * 0.5); //四捨五入
    let showData = data.map(item => {
        return { name: item.name, value: item.value + additional };
    });

    return { largen_data: showData, original_sum: sum, additional: additional };
}

function searchRangeProfit() {
    let first = document.getElementById("profitFirst").value;
    let last = document.getElementById("profitLast").value;

    if (first == "" || last == "") {
        alert("請先輸入日期!");
        return;
    }

    let str = "first=" + first + "&last=" + last;
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Report/searchRangeProfit?' + str,
        type: 'POST',
        async: false,
        error: function (xhr) {
            alert("Ajax發生錯誤!");
        },
        success: function (response) {
            //console.log(response.data);
            if (response.data.length == 0) {
                alert("此區間尚無利潤分析!");
            }
            let option6 = {
                title: {
                    text: '利潤分析',
                    textStyle: {
                        color: '#fff'
                    }
                },
                textStyle: {
                    color: '#fff'
                },
                tooltip: {
                    trigger: 'axis'
                },
                toolbox: {
                    show: true,
                    feature: {
                        saveAsImage: { show: true }
                    }
                },
                legend: {
                    orient: 'horizontal',
                    x: 'center',
                    y: '0',
                    padding: 10,
                    itemGap: 20,
                    textStyle: { color: '#c3cad9' }
                },

                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: getProfitSimpleDate(response.data)
                },
                yAxis: {
                    type: 'value'
                },
                dataZoom: [
                    {   //默認控制xAxis
                        type: 'slider', //slider 型
                        start: 0,      // 左邊在 10% 的位置。
                        end: 100         // 右邊在 60% 的位置。
                    }
                ],
                dataZoom: [
                    {   //默認控制xAxis
                        type: 'slider', //slider 型
                        start: 0,      // 左邊在 10% 的位置。
                        end: 100         // 右邊在 60% 的位置。
                    }
                ],
                series: [
                    {
                        name: "營業額",
                        type: 'line',
                        smooth: true,
                        data: parseProfitData(response.data)["turnover"],
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        }
                    },

                    {
                        name: "銷貨成本",
                        type: 'line',
                        smooth: true,
                        data: parseProfitData(response.data)["cost_of_good"],
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        }
                    },
                    {
                        name: "銷貨費用",
                        type: 'line',
                        smooth: true,
                        data: parseProfitData(response.data)["selling_cost"],
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        }
                    },
                    {
                        name: "營運淨利",
                        type: 'line',
                        smooth: true,
                        data: parseProfitData(response.data)["profit"],
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        }
                    },
                ]
            };

            let mychart6 = echarts.init(document.getElementById("chartmain-6"));
            mychart6.setOption(option6);
        }
    });
}

function getProfitSimpleDate(data) {
    let res = [];
    data.forEach(item => {
        res.push(item.year + "/" + item.month);
    });

    return res;
}
function parseProfitData(data) { //data --> profits
    let turnover = [];
    let cost_of_good = [];
    let selling_cost = [];
    let profit = [];

    data.forEach(item => {
        infos = JSON.parse(item.jsonprofit);
        turnover.push({ name: infos[0]["key"], value: infos[0]["value"] });
        cost_of_good.push({ name: infos[1]["key"], value: infos[1]["value"] });
        selling_cost.push({ name: infos[2]["key"], value: infos[2]["value"] });
        profit.push({ name: infos[3]["key"], value: infos[3]["value"] });
    });

    return { turnover: turnover, cost_of_good: cost_of_good, selling_cost: selling_cost, profit: profit };
}

//原型鏈
String.prototype.changeDatePos = function () {
    //2021 05 22 -> 05-22-2021
    let strlist = this.split("-");
    let newlist = [];
    newlist.push(strlist[1]);
    newlist.push(strlist[2]);
    newlist.push(strlist[0]);

    let newstr = newlist.join("-");
    return newstr;
}

String.prototype.showSimpleDate = function () {
    // 05 22 2021 22:39:45 -> 2021/5/22
    let tmplist = this.split("-");
    tmplist.pop();
    let newlist = tmplist.map(x => parseInt(x)); //[5,22,2021]
    let newstr = newlist[2] + "/" + newlist[0].toString() + "/" + newlist[1].toString();

    return newstr;
}

String.prototype.showDay = function () {
    let tmplist = this.split("-");
    tmplist.pop();
    let newdate = new Date(tmplist.join("/"));
    let day = ['日', '一', '二', '三', '四', '五', '六'];
    let thisday = day[newdate.getDay()];

    return thisday;
}

String.prototype.showDay2 = function () {
    let tmplist = this.split("-");
    let newdate = new Date(tmplist.join("/"));
    let day = ['日', '一', '二', '三', '四', '五', '六'];
    let thisday = day[newdate.getDay()];

    return thisday;
}

//將 base64 文字轉換成 byte 陣列才可以儲存到 Blob 物件
function base64ToArrayBuffer(base64) {

    //window.atob 在 IE 瀏覽器不支援
    var binaryString = window.atob(base64);

    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen); //每個字節 = 8位元 ， 創建一個裝raw binary data的容器(arraybuffer)
    for (var i = 0; i < binaryLen; i++) {
        var ascii = binaryString.charCodeAt(i); //unicode編碼
        bytes[i] = ascii;
    }
    return bytes;
}

String.prototype.base64toURL = function () {
    var blob = new Blob([base64ToArrayBuffer(this.toString())], { type: 'application/octet-stream' });
    return URL.createObjectURL(blob);
}
