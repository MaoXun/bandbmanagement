var page_num = 0;
var global_customers = []; //List

$(document).ready(  //ready function --> wait for html DOM completed
    function customerPageLoaded() {
        if (sessionStorage.getItem('isCollapsed') === 'true') {
            collapse();
        }
        let realCustomers = []; // not deleted customers
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Customer/list',  //獲取客戶資訊api
            type: 'GET',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    if (!response.data[i].isDelete) {
                        realCustomers.push(response.data[i]);
                    }
                }

                global_customers.pop();
                global_customers.push(realCustomers); //將response存入全域變數            

                //console.log(response)
                document.getElementById('customerlist').innerHTML = '';
                let len = (realCustomers.length >= 10) ? 10 : realCustomers.length;
                for (let i = 0; i < len; i++) {
                    document.getElementById('customerlist').insertAdjacentHTML('beforeEnd', `            
                        <tr id="${realCustomers[i].customer_ID}">
                            <td>${realCustomers[i].name}</td>
                            <td>${realCustomers[i].phone_number}</td>
                            <td>${realCustomers[i].Line_id}</td> 
                            <td>${realCustomers[i].email}</td>
                            <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordC_id('${realCustomers[i].customer_ID}')"></i> <i class="fas fa-trash-alt" onclick="deleteCustomer('${realCustomers[i].name}','${realCustomers[i].customer_ID}') "></i></td>
                        </tr>                   
                    `);
                }

                if (realCustomers.length % 10 == 0) {
                    page_num = realCustomers.length / 10;
                } else {
                    page_num = parseInt(realCustomers.length / 10) + 1;
                }

                pageEffect(page_num, 1);
            }
        });
    }
);

//ajax 4type (post delete put get) --> (增 刪 修 查)
function customerSearch() {
    const text = document.getElementById("clientsearch").value;
    let searchPageNum = 0;
    let realSearchCustomers = [];
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Customer/search?clue=' + text,
        type: 'GET',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            //console.log(response);

            //console.log(`<test ${searchCustomers}></test>`);       
            //console.log("searchnum " + searchPageNum);

            for (let i = 0; i < response.data.length; i++) {
                {
                    if (!response.data[i].isDelete) {
                        realSearchCustomers.push(response.data[i]);
                    }
                }
            }
            global_customers.pop(); //清空全域顧客列表
            global_customers.push(realSearchCustomers);

            document.getElementById('customerlist').innerHTML = '';
            if (realSearchCustomers.length > 0) {
                for (let i = 0; i < 10; i++) {
                    if (i < realSearchCustomers.length) {
                        document.getElementById('customerlist').insertAdjacentHTML('beforeEnd', `            
                        <tr id="${realSearchCustomers[i].customer_ID}">
                            <td>${realSearchCustomers[i].name}</td>
                            <td>${realSearchCustomers[i].phone_number}</td>
                            <td>${realSearchCustomers[i].Line_id}</td> 
                            <td>${realSearchCustomers[i].email}</td>
                            <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordC_id('${realSearchCustomers[i].customer_ID}')"></i> <i class="fas fa-trash-alt" onclick="deleteCustomer('${realSearchCustomers[i].name}','${realSearchCustomers[i].customer_ID}') "></i></td>
                        </tr>                   
                        `);
                    } else {
                        break;
                    }
                }

                if (realSearchCustomers.length % 10 == 0) {
                    searchPageNum = realSearchCustomers.length / 10;
                } else {
                    searchPageNum = parseInt((realSearchCustomers.length / 10) + 1);
                }
                pageEffect(searchPageNum, 1);
            } else {
                alert('查無此客戶！');
                location.reload();
            }
        }
    });
}

function closeadd() {
    const name = document.getElementById("customername");
    const phone = document.getElementById("customerphone");
    const line = document.getElementById("customerLINE");
    const email = document.getElementById("customeremail");

    name.value = '';
    phone.value = '';
    line.value = '';
    email.value = '';
}

function addCustomer() {
    const addname = document.getElementById("customername");
    const addphone = document.getElementById("customerphone");
    const addline = document.getElementById("customerLINE");
    const addemail = document.getElementById("customeremail");

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Customer/add',  //獲取客戶資訊api
        type: 'POST',
        data: {
            name: addname.value,
            phone_number: addphone.value,
            Line_id: addline.value,
            email: addemail.value
        },
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            addname.value = '';
            addphone.value = '';
            addline.value = '';
            addemail.value = '';
            location.reload();
        }
    });
}

function deleteCustomer(name, c_id) {
    let customerTrTag = document.getElementById(c_id);
    let text = c_id;
    isRemove = confirm("確定移除 '" + name + "' ？");
    if (isRemove) {
        customerTrTag.remove(); //有取消、確認兩按鈕
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Customer/delete?customer_ID=' + text,  //獲取客戶資訊api
            type: 'PUT', //改成用isDelete 而不是直接刪除
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                alert(response.message);
                location.reload();
            }
        });

        // location.reload();
    }
}

var tempc_id = '';
function recordC_id(c_id) {
    tempc_id = c_id;
    //console.log(tempc_id);
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Customer/search?clue=' + tempc_id,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            document.getElementById("editcustomername").value = response.data[0].name;
            document.getElementById("editcutomerphone").value = response.data[0].phone_number;
            document.getElementById("editcutomerLINE").value = response.data[0].Line_id;
            document.getElementById("editcutomeremail").value = response.data[0].email;
        }
    });
} function editCustomer() {
    //console.log("edit " + tempc_id);
    const customerName = document.getElementById("editcustomername").value;
    const phone = document.getElementById("editcutomerphone").value;
    const line = document.getElementById("editcutomerLINE").value;
    const email = document.getElementById("editcutomeremail").value;

    let customerInfo = {
        customer_ID: tempc_id,
        phone_number: phone,
        Line_id: line,
        name: customerName,
        email: email
    };

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Customer/edit',
        type: 'POST',
        data: customerInfo,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });
}

/*
var 全域皆可用
let 區塊(可能被重新賦值)(常用在{}()loop中)
const 區塊(不會改變的值)
*/


function pageEffect(totalpages, presentpage) { //頁碼按鈕
    //console.log(offeredCustomers_1);
    //console.log(presentpage);
    //console.log(totalpages);

    const customerPageul = document.querySelector('#customerPageul');
    let customerPageli = '';
    let beforePages = presentpage - 1;
    let afterPages = presentpage + 1;
    let activeli;
    if (presentpage > 1) {
        customerPageli += `<li class="prev btn" onclick="pageEffect(${totalpages}, ${presentpage - 1}); changePage(${presentpage - 1}) "><span><i class="fas fa-chevron-left"></i>Prev</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage > 2) { //1 2 (3) 4
            customerPageli += `<li class="num btn" onclick="pageEffect(${totalpages}, 1); changePage(1) "><span>1</span></li>`;
            if (presentpage > 3) {
                customerPageli += `<li class="dots"><span>...</span></li>`;
            }
        }
    }

    if (presentpage == totalpages - 1) {
        beforePages = beforePages - 1;
    } else if (presentpage == totalpages) {
        beforePages = beforePages - 2;
    }

    if (presentpage == 2) {
        afterPages = afterPages + 1;
    } else if (presentpage == 1) {
        afterPages = afterPages + 2;
    }


    for (let i = beforePages; i <= afterPages; i++) { //以presentpage為中心
        if (i > totalpages) {
            break;
        }
        if (i < 1) {
            continue;
        }
        if (presentpage == i) {
            activeli = "active-page";
        } else {
            activeli = "";
        }
        customerPageli += `<li class="num btn ${activeli}" onclick="pageEffect(${totalpages}, ${i}); changePage(${i}) "><span>${i}</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage < totalpages - 1) { //17 (18) 19 20
            if (presentpage < totalpages - 2) {
                customerPageli += `<li class="dots"><span>...</span></li>`;
            }
            customerPageli += `<li class="num btn"  onclick="pageEffect(${totalpages}, ${totalpages}); changePage(${totalpages})"><span>${totalpages}</span></li>`;
        }
    }

    if (presentpage < totalpages) {
        customerPageli += `<li class="next btn" onclick="pageEffect(${totalpages}, ${presentpage + 1}); changePage(${presentpage + 1})"><span>Next<i class="fas fa-chevron-right"></i></span></li>`;
    }
    customerPageul.innerHTML = customerPageli;
}

function changePage(targetpage) {
    let firstDataIndex = (targetpage - 1) * 10 + 1 - 1;
    let trtdTag = '';

    const tbodyTag = document.getElementById("customerlist");

    tbodyTag.innerHTML = '';
    for (let i = firstDataIndex; i < firstDataIndex + 10; i++) {
        if (i < global_customers[0].length) {
            trtdTag += `<tr id="${global_customers[0][i].customer_ID}">
                            <td>${global_customers[0][i].name}</td>
                            <td>${global_customers[0][i].phone_number}</td>
                            <td>${global_customers[0][i].Line_id}</td>
                            <td>${global_customers[0][i].email}</td>
                            <td><i class="fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordC_id('${global_customers[0][i].customer_ID}')" ></i><i class="fas fa-trash-alt" onclick="deleteCustomer('${global_customers[0][i].name}', '${global_customers[0][i].customer_ID}')"></i></td>
                        </tr>`;
        }
    }
    tbodyTag.innerHTML = trtdTag;
    //console.log(global_customers[0][1].name);
}

