var global_orders = [];
var packages = [];
var products = [];

$(document).ready(
    function orderPageLoad() {
        if (sessionStorage.getItem('isCollapsed') === 'true') {
            console.log(sessionStorage.getItem('isCollapsed'));
            collapse();
        }

        let real_orders = [];
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Order/getOrderlist',  //獲取客戶資訊api
            type: 'GET',
            async: false,
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    if (!response.data[i].isDelete) {
                        real_orders.push(response.data[i]);
                    }
                }
                global_orders.pop();
                global_orders.push(real_orders);

                document.getElementById('orderlist').innerHTML = '';
                let len = (real_orders.length >= 10) ? 10 : real_orders.length;
                for (let i = 0; i < len; i++) {
                    document.getElementById('orderlist').insertAdjacentHTML('beforeEnd', `            
                        <tr id="${real_orders[i].order.order_ID}">
                            <td>${real_orders[i].order.order_number}</td>
                            <td>${real_orders[i].customer.name}</td>
                            <td>${real_orders[i].customer.phone_number}</td>
                            <td>${real_orders[i].order.people_num}</td>
                            <td>${real_orders[i].order.total_money}</td>
                            <td>${real_orders[i].order.deposit}</td>
                            <td><i class="action fas fa-info-circle" onclick="orderInfo('${real_orders[i].order.order_number}')"></i></td>
                            <td><i class="action fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordO_id('${real_orders[i].order.order_number}')"></i> <i class="fas fa-trash-alt" onclick="deleteOrder('${real_orders[i].order.order_ID}') "></i></td>
                        </tr>                   
                    `);
                }

                if (real_orders.length % 10 == 0) {
                    page_num = real_orders.length / 10;
                } else {
                    page_num = parseInt(real_orders.length / 10) + 1;
                }

                pageEffect(page_num, 1);
            }
        });

        //取得套裝列表
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Package/getPackageslist',
            type: 'GET',
            async: false,
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    if (!response.data[i].isDelete) {
                        packages.push(response.data[i]);
                    }
                }
            }
        });

        //取得商品列表
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Product/list',
            type: 'GET',
            async: false,
            contentType: 'application/x-www-form-urlencoded; charset=utf-8',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                for (let i = 0; i < response.data.length; i++) {
                    if (!response.data[i].isDelete) {
                        products.push(response.data[i]);
                    }
                }
            }
        });
    }
);

function orderSearch() {
    let clue = document.getElementById("orderSearch").value;
    let searchPageNum = 0;
    let realSearchOrders = [];
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Order/searchOrder?clue=' + clue,
        type: 'GET',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {

            if (response.data === null) {
                alert(response.message);
                return;
            } else {
                for (let i = 0; i < response.data.length; i++) {
                    {
                        if (!response.data[i].isDelete) {
                            realSearchOrders.push(response.data[i]);
                        }
                    }
                }
            }

            global_orders.pop();
            global_orders.push(realSearchOrders[0]);

            // console.log(realSearchOrders[0].order);
            document.getElementById('orderlist').innerHTML = '';

            let len = 0;
            if (realSearchOrders.length < 10) {
                len = realSearchOrders.length;
            } else {
                len = 10;
            }

            if (realSearchOrders.length > 0) {
                for (let i = 0; i < len; i++) {
                    document.getElementById('orderlist').insertAdjacentHTML('beforeEnd', `            
                        <tr id="${realSearchOrders[i].order.order_ID}">
                            <td>${realSearchOrders[i].order.order_number}</td>
                            <td>${realSearchOrders[i].customer.name}</td>
                            <td>${realSearchOrders[i].customer.phone_number}</td>
                            <td>${realSearchOrders[i].order.people_num}</td>
                            <td>${realSearchOrders[i].order.total_money}</td>
                            <td>${realSearchOrders[i].order.deposit}</td>
                            <td><i class="action fas fa-info-circle" onclick="orderInfo('${realSearchOrders[i].order.order_number}')"></i></td>
                            <td><i class="action fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordO_id('${realSearchOrders[i].order.order_number}')"></i> <i class="fas fa-trash-alt" onclick="deleteOrder('${realSearchOrders[i].order.order_ID}') "></i></td>
                        </tr>                        
                        `);
                }
                if (realSearchOrders.length % 10 == 0) {
                    searchPageNum = realSearchOrders.length / 10;
                } else {
                    searchPageNum = parseInt((realSearchOrders.length / 10) + 1);
                }
                pageEffect(searchPageNum, 1);
            } else {
                alert('查無此產品！');
                location.reload();
            }
        }
    });
}

function addOrder() {
    let customer_name = document.getElementById("addcutomername");
    let addphone = document.getElementById("addphone");
    let addtotal_money = document.getElementById("addtotal_money");
    let adddeposit = document.getElementById("adddeposit");

    let addcash = document.getElementById("addcash");
    let addpeople_num = document.getElementById("addpeople_num");
    let addadditional = document.getElementById("addadditional");

    // let adddepositdate = document.getElementById("adddepositdate");

    // let addcheckindate = document.getElementById("addcheckindate");

    /*檢查必填、合格*/
    if (customer_name.value.trim() === '') {
        customer_name.nextElementSibling.setAttribute('style', 'display: block');
        customer_name.setAttribute('style', 'border-color: red');
    } else {
        customer_name.nextElementSibling.setAttribute('style', 'display: none');
        customer_name.setAttribute('style', 'border-color: #ced4da');
    }

    /*組裝物件*/
    let check = [checkValid(addphone), checkValid(adddeposit), checkValid(addcash), checkValid(addpeople_num, true), checkValid(addadditional), checkValid(addtotal_money)];
    for (let i = 0; i < check.length; i++) {
        if (check[i] && i == check.length - 1) {
            let test_add = document.getElementsByClassName("addproduct");
            if (test_add.length > 0) {
                encapsulate();
            } else {
                alert("尚未新增任何購買細項！");
                return;
            }
        } else if (!check[i]) {
            return;
        }
    }

    /*組裝成一包物件並發ajax*/
    let addorder = {
        order_info: order_info,
        details_info: details_info
    };

    //console.log(addorder);
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Order/addOrder',
        type: 'POST',
        async: false,
        data: addorder,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });
}

var order_info = [];
var details_info = [];
function encapsulate() {
    /*上半部物件*/
    order_info = {
        customer_name: document.getElementById("addcutomername").value,
        phone: document.getElementById("addphone").value,
        total_money: document.getElementById("addtotal_money").value,
        deposit: document.getElementById("adddeposit").value,
        people_num: document.getElementById("addpeople_num").value,
        cash_income: document.getElementById("addcash").value,
        additional_purchase: document.getElementById("addadditional").value,
        deposit_date: document.getElementById("adddepositdate").value,
        checkinDate: document.getElementById("addcheckindate").value
    };

    /*下半部陣列*/
    details_info = [];
    document.querySelectorAll(".addproduct").forEach(element => {
        let p_id = searchId(element.querySelector(".second-part > button").innerText, 1);
        let c_id = searchId(element.querySelector(".third-part > button").innerText, 2);
        let detail = {
            package: p_id === undefined ? "" : p_id,
            product: c_id === undefined ? "" : c_id,
            num: element.querySelector(".fourth-part > button").innerText,
            isPackage: element.querySelector(".second-part > button").innerText === '無' ? false : true
        }
        details_info.push(detail);
    });
}

function resetTotal() {
    let total = document.getElementById("addtotal_money");
    let addcash = document.getElementById("addcash");
    let adddeposit = document.getElementById("adddeposit");
    let addadditional = document.getElementById("addadditional");

    total.value = Number(addcash.value) + Number(adddeposit.value) + Number(addadditional.value);
}

function openAddForm() {
    count = 0;
    document.querySelectorAll('.order-form-item').forEach(element => {
        element.value = '';
        element.setAttribute('style', 'border-color: #ced4da');
        element.nextElementSibling.setAttribute('style', 'display: none');
    });
    document.querySelectorAll('.addproduct').forEach(element => { element.remove() });
    realcount = 0;
}

var count = 1;
var realcount = 0;
function addProducts() {
    count++;
    let form = document.getElementById("add_order_form");
    let thisid = `add${count}`;
    form.insertAdjacentHTML('beforeend', `
            <div id="${thisid}" class="addproduct">
                <h4 class="first-part">${realcount + 1}</h4>
                <div class="btn-group second-part">
                    <button id="package_dropdown${count}" type="button"
                        class="btn btn-primary dropdown-toggle package_dropdown" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        套裝行程
                    </button>
                    <div class="dropdown-menu package-dropdown-menu">
                        <a class="dropdown-item" onclick="clickPackageDropdownItem('無', ${count})"
                        href="#">無</a>  
                        <div class="dropdown-divider"></div>         
                    </div>
                </div>
                <div class="btn-group  third-part">
                    <button id="product_dropdown${count}" type="button"
                        class="btn btn-success dropdown-toggle product_dropdown" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        單一商品
                    </button>
                    <div class="dropdown-menu product-dropdown-menu">
                        <a class="dropdown-item" onclick="clickProductDropdownItem('無', ${count})"
                        href="#">無</a>
                        <div class="dropdown-divider"></div>
                    </div>
                </div>
                <div class="btn-group fourth-part">
                    <button id="products-num${count}" type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">1</button>
                    <div id="products-num-dropdown${count}" class="dropdown-menu">
            
                    </div>
                </div>
                <i class="fas fa-trash-alt fourth-part" onclick="deleteAddProducts('${thisid}')"></i>
            </div>
    `);
    realcount++;

    for (let i = 1; i <= 10; i++) {
        document.getElementById(`products-num-dropdown${count}`).insertAdjacentHTML("beforeend", `
            <a class="dropdown-item" href="#" onclick="clickNumDropDownItem('${i}','${count}')">${i}</a>
        `);
    }

    for (let i = 0; i < packages.length; i++) {
        let rows = document.getElementsByClassName("package-dropdown-menu");
        rows[rows.length - 1].insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" onclick="clickPackageDropdownItem('${packages[i].package_type}', ${count})"
            href="#">${packages[i].package_type}</a>
        `);
    }

    for (let i = 0; i < products.length; i++) {
        let rows = document.getElementsByClassName("product-dropdown-menu");
        rows[rows.length - 1].insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" onclick="clickProductDropdownItem('${products[i].name}', ${count})"
            href="#">${products[i].name}</a>
        `);
    }
}

function deleteAddProducts(addid) {
    let will_delete = document.getElementById(addid);
    will_delete.parentNode.removeChild(will_delete);
    realcount--;
    /* 重新分配編號 */
    let numbers = document.getElementsByClassName("first-part");
    for (let i = 0; i < realcount; i++) {
        numbers[i].innerText = i + 1;
    }
}

var tempo_num = '';
function recordO_id(o_id) {
    tempo_num = o_id;
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Order/searchOrder?clue=' + tempo_num,
        type: 'GET',
        contentType: 'application/x-www-form-urlencoded; charset=utf-8',
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            document.getElementById("editcustomername").value = response.data[0].customer.name;
            document.getElementById("editphone").value = response.data[0].customer.phone_number;
            document.getElementById("edittotalmoney").value = response.data[0].order.total_money;
            document.getElementById("editdeposit").value = response.data[0].order.deposit;
            document.getElementById("editpeople_num").value = response.data[0].order.people_num;
        }
    });
}
function editOrder() {
    const customerName = document.getElementById("editcustomername").value;
    const phone = document.getElementById("editphone").value;
    const totalmoney = document.getElementById("edittotalmoney").value;
    const deposit = document.getElementById("editdeposit").value;
    const people_num = document.getElementById("editpeople_num").value;

    console.log(tempo_num);

    let editOrder = {
        ordernumber: tempo_num,
        total_money: totalmoney,
        deposit: deposit,
        customer_name: customerName,
        phone: phone,
        people_num: people_num
    };

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Order/editOrder',
        type: 'POST',
        data: editOrder,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            console.log(response);
            alert(response.message);
            location.reload();
        }
    });
}

function clickPackageDropdownItem(item_text, countid) {
    let package_dropdown = document.getElementById(`package_dropdown${countid}`);
    package_dropdown.innerText = item_text;

    let product_dropdown = document.getElementById(`product_dropdown${countid}`);
    product_dropdown.innerText = '無';
}

function clickProductDropdownItem(item_text, countid) {
    let product_dropdown = document.getElementById(`product_dropdown${countid}`);
    product_dropdown.innerText = item_text;

    let package_dropdown = document.getElementById(`package_dropdown${countid}`);
    package_dropdown.innerText = '無';
}

function clickNumDropDownItem(item_text, countid) {
    let numdrop = document.getElementById(`products-num${countid}`);
    numdrop.innerText = item_text;
}

var previousHTML = "";
var previousStyle;
var new_lower_info = [];
var order_detail_info = [];
function orderInfo(order_number) {
    previousHTML = document.getElementById("pagecontent").innerHTML;
    previousStyle = document.getElementById("pagecontent").style;
    document.getElementById("pagecontent").innerHTML = "";
    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Order/getOrderInfo?order_number=' + order_number,
        type: 'POST',
        async: false,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            order_detail_info = response.data;
        }
    });
    //console.log(order_detail_info);
    new_lower_info = order_detail_info.lowerInfos;

    let tempHTML = "";
    tempHTML += `
        <div class="back" onclick="goBack()"><i class="fas fa-chevron-left"></i>訂單管理</div>
            <h2 style="margin-bottom: 0.5em; font-weight: 400;">細項</h2>
            <h4 style="margin-left: 1em;">訂單編號-${order_detail_info.upperInfo.order_number}</h4>
            <div><button type="button" class="btn btn-success all-confirm" onclick="updateOrderDetail()">確認<i class="fas fa-check"></i></button>
            </div>
            <div id="orderdetail" style="margin: 1.5em 0 0 1.5em">
                <div id="basicinfo">
                    <div class="classifylabel">基本資訊</div>
                    <div class="container-fluid">
                        <div class="row info-row">
                            <div class="col-md-1"><span class="info-label">客戶姓名：</span></div>
                            <div class="col-md-3"><input id="customername" class="form-control info-input" value="${order_detail_info.upperInfo.name}"></div>
                            <div class="col-md-1"><span class="info-label">聯絡電話：</span></div>
                            <div class="col-md-3"><input id="phone" class="form-control info-input" value="${order_detail_info.upperInfo.phone_number}"></div>
                            <div class="col-md-1"><span class="info-label">LINE-ID：</span></div>
                            <div class="col-md-3"><input id="Line" class="form-control info-input" value="${order_detail_info.upperInfo.Line_id}"></div>
                        </div>
                        <div class="row info-row">                              
                            <div class="col-md-1"><span class="info-label">訂金：</span></div>
                            <div class="col-md-3"><input id="deposit" class="form-control info-input" value="${order_detail_info.upperInfo.deposit}"></div>
                            <div class="col-md-1"><span class="info-label">現金：</span></div>
                            <div class="col-md-3"><input id="cash" class="form-control info-input" value="${order_detail_info.upperInfo.cash_income}"></div>
                            <div class="col-md-1"><span class="info-label">總金額：</span></div>
                            <div class="col-md-3"><input id="total" class="form-control info-input" value="${order_detail_info.upperInfo.total_money}"></div>                    
                        </div>
                        <div class="row info-row">
                            <div class="col-md-1"><span class="info-label">加價購：</span></div>
                            <div class="col-md-3"><input id="additional" class="form-control info-input" value="${order_detail_info.upperInfo.additional_purchase}"></div>    
                            <div class="col-md-1"><span class="info-label">人數：</span></div>
                            <div class="col-md-3"><input id="people" class="form-control info-input" value="${order_detail_info.upperInfo.people_num}"></div>                
                            <div class="col-md-1"><span class="info-label">訂金匯入：</span></div>
                            <div class="col-md-3"><input type="date" id="deposit_date" class="form-control info-input" value="${order_detail_info.upperInfo.deposit_date}"></div>
                        </div>
                        <div class="row info-row">
                            <div class="col-md-1"><span class="info-label">訂單時間：</span></div>
                            <div class="col-md-3"><span class="info-label">${order_detail_info.upperInfo.createTime.sortDatePos()}</span></div> 
                            <div class="col-md-1"><span class="info-label">入住時間：</span></div>
                            <div class="col-md-3"><input type="date" id="checkinDate" class="form-control info-input" value="${order_detail_info.upperInfo.checkinDate}"></div>                        
                        </div>
                    </div>
                </div>
                <div id="detailcontent">
                    <div class="classifylabel" style="margin-top: 2em;">訂單細項</div>
                    <table id="detail-table" class="table table-bordered detail-table">
                        <thead class="thead-dark">
                            <tr>
                                <th style="width: 60px; text-align: center; font-size: larger;"><i
                                        class="fas fa-plus plus-detail" data-toggle="modal"
                                        data-target="#add"></i>
                                </th>
                                <th>商品名稱</th>
                                <th>商品類型</th>
                                <th>商品單價</th>
                                <th>訂購數量</th>
                                <th>總價</th>
                                <th>動作</th>
                            </tr>
                        </thead>
                        <tbody>
                           
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- 新增細項 -->
            <div class="modal" id="add" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span><span class="sr-only">
                                        Close</span></button>
                                <h5 class="modal-title">新增細項</h5>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="btn-group" style="width: 45%">
                                        <button id="package_dropdown_add_btn" type="button" class="btn btn-primary dropdown-toggle package_dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            套裝行程
                                        </button>
                                        <div id="package_dropdown_add" class="dropdown-menu package-dropdown-menu">
                                            <a class="dropdown-item" href="#">無</a>  
                                            <div class="dropdown-divider"></div>         
                                        </div>
                                    </div>
                                    <div class="btn-group" style="width: 45%">
                                        <button id="product_dropdown_add_btn" type="button" class="btn btn-success dropdown-toggle  product_dropdown"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        單一商品
                                        </button>
                                        <div id="product_dropdown_add" class="dropdown-menu product-dropdown-menu">
                                            <a class="dropdown-item" href="#">無</a>  
                                            <div class="dropdown-divider"></div>         
                                        </div>   
                                    </div>                                                         
                                    <div class="btn-group">
                                        <button id="products-num-add-btn" type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">1</button>
                                        <div id="products-num-add" class="dropdown-menu">
                                
                                        </div>
                                    </div> 
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"
                                >關閉</button>
                                <button type="button" class="btn btn-primary" onclick="addDetail()">新增</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- 編輯細項 -->
                <div class="modal" id="edit" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
                    aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">&times;</span><span class="sr-only">
                                        Close</span></button>
                                <h5 class="modal-title">編輯細項</h5>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="btn-group" style="width: 45%">
                                        <button id="package_dropdown_edit_btn" type="button" class="btn btn-primary dropdown-toggle package_dropdown"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            套裝行程
                                        </button>
                                        <div id="package_dropdown_edit" class="dropdown-menu package-dropdown-menu">
                                            <a class="dropdown-item" href="#">無</a>  
                                            <div class="dropdown-divider"></div>         
                                        </div>
                                    </div>
                                    <div class="btn-group" style="width: 45%">
                                        <button id="product_dropdown_edit_btn" type="button" class="btn btn-success dropdown-toggle  product_dropdown"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        單一商品
                                        </button>
                                        <div id="product_dropdown_edit" class="dropdown-menu product-dropdown-menu">
                                            <a class="dropdown-item" href="#">無</a>  
                                            <div class="dropdown-divider"></div>         
                                        </div>   
                                    </div>                                                         
                                    <div class="btn-group">
                                        <button id="products-num-edit-btn" type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">1</button>
                                        <div id="products-num-edit" class="dropdown-menu">
                                
                                        </div>
                                    </div> 
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"
                                >關閉</button>
                                <button type="button" class="btn btn-primary" onclick="editDetail()">編輯</button>
                            </div>
                        </div>
                    </div>
                </div>
    `;
    document.getElementById("pagecontent").innerHTML = tempHTML;

    for (let i = 0; i < order_detail_info.lowerInfos.length; i++) {

        let type = "";
        if (order_detail_info.lowerInfos[i].product_type === 1) {
            type = "單一商品";
        } else {
            type = "套裝行程";
        }

        document.querySelector("#detail-table > tbody").insertAdjacentHTML('beforeend', `
            <tr>
                <th>${i + 1}</th>
                <th>${order_detail_info.lowerInfos[i].products_name}</th>
                <th>${type}</th>
                <th>${order_detail_info.lowerInfos[i].unit_price}</th>
                <th>${order_detail_info.lowerInfos[i].num}</th>
                <th>${order_detail_info.lowerInfos[i].total_money}</th>
                <th class="action"><i class="fas fa-edit" data-toggle="modal"
                        data-target="#edit" onclick="recordDetailIndex('${i}')"></i><i class="fas fa-trash-alt" onclick="deleteDetail('${i}')"></i></th>
            </tr>
        `);
    }

    for (let i = 0; i < packages.length; i++) {
        document.getElementById("package_dropdown_add").insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" href="#" onclick="clickPackageDropdownItem('${packages[i].package_type}', '_add_btn')">${packages[i].package_type}</a>  
        `);

        document.getElementById("package_dropdown_edit").insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" href="#" onclick="clickPackageDropdownItem('${packages[i].package_type}', '_edit_btn')">${packages[i].package_type}</a>  
        `);
    }

    for (let i = 0; i < products.length; i++) {
        document.getElementById("product_dropdown_add").insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" href="#" onclick="clickProductDropdownItem('${products[i].name}', '_add_btn')">${products[i].name}</a>  
        `);

        document.getElementById("product_dropdown_edit").insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" href="#" onclick="clickProductDropdownItem('${products[i].name}', '_edit_btn')">${products[i].name}</a>  
        `);
    }

    for (let i = 1; i <= 10; i++) {
        document.getElementById("products-num-add").insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" href="#" onclick="clickNumDropDownItem('${i}', '-add-btn')">${i}</a>  
        `);

        document.getElementById("products-num-edit").insertAdjacentHTML('beforeend', `
            <a class="dropdown-item" href="#" onclick="clickNumDropDownItem('${i}', '-edit-btn')">${i}</a>  
        `);
    }
}

function addDetail() {
    let package = document.getElementById("package_dropdown_add_btn").innerText;
    let product = document.getElementById("product_dropdown_add_btn").innerText;
    let num = document.getElementById("products-num-add-btn").innerText;
    let type = 0;
    let name = "";
    let price = 0;
    let id = '';

    if (package === "無" && product === "無") {
        alert("請選擇商品！");
    } else if (package === "無" && product != "無") {
        type = 1;
        name = product;
        products.forEach(element => {
            if (element.name === name) {
                price = element.price;
                id = element.commodity_ID;
                return;
            }
        });
    } else {
        type = 2;
        name = package;
        packages.forEach(element => {
            if (element.package_type === name) {
                price = element.price;
                id = element.package_ID;
                return;
            }
        });
    }

    new_lower_info.push({
        num: num,
        product_type: type,
        products_name: name,
        products_ID: id,
        total_money: price * num,
        unit_price: price
    });

    drawTable();
}

function deleteDetail(index) {
    new_lower_info.splice(index, 1); //接合
    drawTable();
}

var tempindex = '';
function recordDetailIndex(index) {
    tempindex = index;
    let detail = new_lower_info[tempindex];
    if (detail.product_type === 1) {
        document.getElementById("package_dropdown_edit_btn").innerText = '無';
        document.getElementById("product_dropdown_edit_btn").innerText = detail.products_name;
    } else {
        document.getElementById("package_dropdown_edit_btn").innerText = detail.products_name;
        document.getElementById("product_dropdown_edit_btn").innerText = '無';
    }
    document.getElementById("products-num-edit-btn").innerText = detail.num;
}
function editDetail() {
    let package = document.getElementById("package_dropdown_edit_btn").innerText;
    let product = document.getElementById("product_dropdown_edit_btn").innerText;
    let will_edit_detail = new_lower_info[tempindex];

    if (package === '無' && product === '無') {
        alert("請選擇商品！");
    } else if (package === '無' && product != '無') {
        will_edit_detail.products_name = product;
        products.forEach(element => {
            if (element.name === product) {
                will_edit_detail.unit_price = element.price;
                will_edit_detail.products_ID = element.commodity_ID;
            }
        });
        will_edit_detail.product_type = 1;
    } else {
        will_edit_detail.products_name = package;
        packages.forEach(element => {
            if (element.package_type === package) {
                will_edit_detail.unit_price = element.price;
                will_edit_detail.products_ID = package_ID;
            }
        });
        will_edit_detail.product_type = 2;
    }
    will_edit_detail.num = document.getElementById("products-num-edit-btn").innerText;
    will_edit_detail.total_money = will_edit_detail.unit_price * will_edit_detail.num;

    drawTable();
}

function deleteOrder(order_ID) {
    let yes = confirm("確定刪除?");
    if (yes) {
        $.ajax({
            url: 'https://makatabar.com/BandBManagement/api/Order/deleteOrder?o_id=' + order_ID,
            type: 'POST',
            error: function (xhr) {
                alert('Ajax request 發生錯誤');
            },
            success: function (response) {
                alert(response.message);
                location.reload();
            }
        });
    } else {
        return;
    }

}

function updateOrderDetail() {
    if (new_lower_info.length === 0) {
        alert("未選擇任何商品！");
        return;
    }
    if (parseInt(document.getElementById("total").value) < 0 || parseInt(document.getElementById("deposit").value) < 0) {
        alert("金額不可為負數！");
        return;
    }

    let newupperinfo = {
        order_ID: order_detail_info.upperInfo.order_ID,
        order_number: order_detail_info.upperInfo.order_number,
        total_money: document.getElementById("total").value,
        deposit: document.getElementById("deposit").value,
        people_num: document.getElementById("people").value,
        cash_income: document.getElementById("cash").value,
        additional_purchase: document.getElementById("additional").value,
        createTime: order_detail_info.upperInfo.createTime,
        name: document.getElementById("customername").value,
        phone_number: document.getElementById("phone").value,
        Line_id: document.getElementById("Line").value,
        deposit_date: document.getElementById("deposit_date").value,
        checkinDate: document.getElementById("checkinDate").value
    };
    // the new lower infos is new_lower_info declared before

    let detail_Info = {
        upperInfo: newupperinfo,
        lowerInfos: new_lower_info
    }

    $.ajax({
        url: 'https://makatabar.com/BandBManagement/api/Order/updateOrderDetailInfo',
        type: 'POST',
        async: false,
        data: detail_Info,
        error: function (xhr) {
            alert('Ajax request 發生錯誤');
        },
        success: function (response) {
            alert(response.message);
            location.reload();
        }
    });
}

function pageEffect(totalpages, presentpage) {

    const orderPageul = document.querySelector('#orderPageul');
    let orderPageli = '';
    let beforePages = presentpage - 1;
    let afterPages = presentpage + 1;
    let activeli;
    if (presentpage > 1) {
        orderPageli += `<li class="prev btn" onclick="pageEffect(${totalpages}, ${presentpage - 1}); changePage(${presentpage - 1}) "><span><i class="fas fa-chevron-left"></i>Prev</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage > 2) { //1 2 (3) 4
            orderPageli += `<li class="num btn" onclick="pageEffect(${totalpages}, 1); changePage(1) "><span>1</span></li>`;
            if (presentpage > 3) {
                orderPageli += `<li class="dots"><span>...</span></li>`;
            }
        }
    }

    if (presentpage == totalpages - 1) {
        beforePages = beforePages - 1;
    } else if (presentpage == totalpages) {
        beforePages = beforePages - 2;
    }

    if (presentpage == 2) {
        afterPages = afterPages + 1;
    } else if (presentpage == 1) {
        afterPages = afterPages + 2;
    }


    for (let i = beforePages; i <= afterPages; i++) { //以presentpage為中心
        if (i > totalpages) {
            break;
        }
        if (i < 1) {
            continue;
        }
        if (presentpage == i) {
            activeli = "active-page";
        } else {
            activeli = "";
        }
        orderPageli += `<li class="num btn ${activeli}" onclick="pageEffect(${totalpages}, ${i}); changePage(${i}) "><span>${i}</span></li>`;
    }

    if (totalpages > 3) {
        if (presentpage < totalpages - 1) { //17 (18) 19 20
            if (presentpage < totalpages - 2) {
                orderPageli += `<li class="dots"><span>...</span></li>`;
            }
            orderPageli += `<li class="num btn"  onclick="pageEffect(${totalpages}, ${totalpages}); changePage(${totalpages}) "><span>${totalpages}</span></li>`;
        }
    }

    if (presentpage < totalpages) {
        orderPageli += `<li class="next btn" onclick="pageEffect(${totalpages}, ${presentpage + 1}); changePage(${presentpage + 1})"><span>Next<i class="fas fa-chevron-right"></i></span></li>`;
    }
    orderPageul.innerHTML = orderPageli;
}

function changePage(targetpage) {
    let firstDataIndex = (targetpage - 1) * 10 + 1 - 1;
    let trtdTag = '';

    const tbodyTag = document.getElementById("orderlist");

    tbodyTag.innerHTML = '';
    for (let i = firstDataIndex; i < firstDataIndex + 10; i++) {
        if (i < global_orders[0].length) {
            trtdTag += `<tr id="${global_orders[0][i].order.order_ID}">
                                <td>${global_orders[0][i].order.order_number}</td>
                                <td>${global_orders[0][i].customer.name}</td>
                                <td>${global_orders[0][i].customer.phone_number}</td>
                                <td>${global_orders[0][i].order.people_num}</td>
                                <td>${global_orders[0][i].order.total_money}</td>                       
                                <td>${global_orders[0][i].order.deposit}</td>
                                <td><i class="action fas fa-info-circle" onclick="orderInfo('${global_orders[0][i].order.order_number}')"></i></td>
                                <td><i class="action fas fa-edit" data-toggle="modal" data-target="#edit" onclick="recordO_id('${global_orders[0][i].order.order_number}')"></i> <i class="fas fa-trash-alt" onclick="deleteOrder('${global_orders[0][i].order.order_ID}') "></i></td>
                            </tr>`;
        }
    }
    tbodyTag.innerHTML = trtdTag;
}

function checkValid(input, people = false) {
    let rules = RegExp('^[0-9]+$');
    let isvaild = true;
    if (input.value.trim() === '') {
        input.nextElementSibling.setAttribute('style', 'display: block');
        input.setAttribute('style', 'border-color: red');
        input.nextElementSibling.innerText = '此為必填欄位';
        isvaild = false;
    } else if (!rules.test(input.value.trim())) {
        input.nextElementSibling.setAttribute('style', 'display: block');
        input.setAttribute('style', 'border-color: red');
        input.nextElementSibling.innerText = '必須為純數字';
        isvaild = false;
    } else {
        if (people) {
            if (Number(input.value) <= 0) {
                input.nextElementSibling.setAttribute('style', 'display: block');
                input.setAttribute('style', 'border-color: red');
                input.nextElementSibling.innerText = '不得小於0';
                isvaild = false;
            }
        } else {
            input.nextElementSibling.setAttribute('style', 'display: none');
            input.setAttribute('style', 'border-color: #ced4da');
        }
    }

    return isvaild;
}

function searchId(text, type) {
    if (type === 1) //package
    {
        for (let i = 0; i < packages.length; i++) {
            if (text === packages[i].package_type) {
                return packages[i].package_ID;
            }
        }
    } else if (type === 2) {
        for (let i = 0; i < products.length; i++) {
            if (text === products[i].name) {
                return products[i].commodity_ID;
            }
        }
    }
}

function drawTable() {
    document.querySelector("#detail-table > tbody").innerHTML = '';
    for (let i = 0; i < new_lower_info.length; i++) {
        let strtype = "";
        if (new_lower_info[i].product_type === 1) {
            strtype = "單一商品";
        } else {
            strtype = "套裝行程";
        }
        document.querySelector("#detail-table > tbody").insertAdjacentHTML('beforeend', `
            <tr>
                <th>${i + 1}</th>
                <th>${new_lower_info[i].products_name}</th>
                <th>${strtype}</th>
                <th>${new_lower_info[i].unit_price}</th>
                <th>${new_lower_info[i].num}</th>
                <th>${new_lower_info[i].total_money}</th>
                <th class="action"><i class="fas fa-edit" data-toggle="modal"
                        data-target="#edit" onclick="recordDetailIndex('${i}')"></i><i class="fas fa-trash-alt" onclick="deleteDetail('${i}')"></i></th>
            </tr>
        `);
    }
}

String.prototype.sortDatePos = function () {
    let list = this.split(" ");
    let newlist = [];
    newlist.push(list[2]);
    newlist.push(list[0]);
    newlist.push(list[1]);
    newlist.push(list[3]);

    let newstr = newlist.join(" ");

    return newstr;
}