﻿using BandBAPI_2.Models;
using BandBAPI_2.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace BandBAPI_2.Controllers
{
    public class LineBotController : ApiController
    {
        BaseDB db = new BaseDB();

        [HttpPost]
        [Route("api/LineBot/WriteinID")]
        public IHttpActionResult POST()
        {
            string ChannelAccessToken = "3xcNGq0rrUtl5hasruR3dg9wZJNoobOqvZM494v63+IOggQ+Dc0n7BPkDUkZ79bzt0f9oloLmXom1OvYU6JlQFJ3BPHhS+3WlkPKSE7+V04WKG+aEcqO/yRARblzrnxY32o8oJjla5zDqK4ylgX6ngdB04t89/1O/w1cDnyilFU=";

            try
            {
                //取得 http Post RawData(should be JSON)
                string postData = Request.Content.ReadAsStringAsync().Result;
                //剖析JSON
                var ReceivedMessage = isRock.LineBot.Utility.Parsing(postData);
                
                if(Regex.IsMatch(ReceivedMessage.events[0].message.text, "^[0-9]{10}已填$"))
                {
                    string phone = ReceivedMessage.events[0].message.text.Substring(0,10);
                    isRock.LineBot.Bot bot = new isRock.LineBot.Bot(ChannelAccessToken);

                    string check = $"select * from Customer where phone_number='{phone}'";
                    List<Customer> checks = db.NewQuery<Customer>(check);

                    if(checks.Count > 0)
                    {
                        //var userInfo = bot.GetUserInfo(ReceivedMessage.events.FirstOrDefault().source.userId);
                        var id = ReceivedMessage.events.FirstOrDefault().source.userId;
                       
                        Customer customer = checks.FirstOrDefault();
                        if (string.IsNullOrEmpty(customer.line_user_id))
                        {
                            customer.line_user_id = id;

                            db.Update(customer);

                            //回覆用戶
                            isRock.LineBot.Utility.ReplyMessage(ReceivedMessage.events[0].replyToken, "資料已建檔！", ChannelAccessToken);
                        }                    
                    }         
                }
                             
                //回覆API OK
                return Ok();
            }
            catch (Exception ex)
            {
                return Ok();
            }
        }

        [HttpPost]
        [Route("api/LineBot/test2")]
        public IHttpActionResult test2()
        {
            return Ok("Ok");
        }

        [HttpPost]
        [Route("api/GoogleSheet/getCustomerData")]
        public void getCustomerData(GoogleSheetCustomer customer)
        {
            string check = $"select * from Customer where name=N'{customer.name}' and phone_number='{customer.phone}'";
            List<Customer> check_cus = db.NewQuery<Customer>(check);
            if(check_cus.Count > 0)
            {
                //do nothing
            }else
            {
                Customer insertCus = new Customer();
                insertCus.name = customer.name;
                insertCus.phone_number = customer.phone;
                insertCus.Line_id = customer.LineID;
                insertCus.email = customer.email;

                string datepattern = "MM-dd-yyyy HH:mm:ss";
                insertCus.createTime = DateTime.Now.ToString(datepattern);

                db.Insert(insertCus);
            }         
        }
    }
}
