﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BandBAPI_2.Controllers
{
    public class TestController : ApiController
    {
        [HttpGet]
        [Route("api/Test/testConnection")]
        public string test()
        {
            return "測試連線";
        }
    }
}
