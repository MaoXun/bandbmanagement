﻿using BandBAPI_2.Models;
using BandBAPI_2.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace BandBAPI_2.Controllers
{
    public class PackageController : ApiController
    {
        BaseDB db = new BaseDB(); 

        [HttpGet]
        [Route("api/Package/getPackageslist")]
        public Response getPackageslist()
        {
            try
            {
                string sql = "select * from Package";
                return new Response(200, "", true, db.NewQuery<Package>(sql));
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpGet]
        [Route("api/Package/search")]
        public Response packageSearch(string clue)
        {
            try
            {
                string sql = "";
                try
                {
                    sql = $@"select * from Package
                            where package_ID='{Guid.Parse(clue)}'";
                }
                catch
                {
                    string pattern = "^[0-9]+$"; //開頭 結尾 都需是數字
                    Regex regex = new Regex(pattern);
                    if (regex.IsMatch(clue))
                    {
                        sql = $@"select * from Package
                                 where price={clue}";
                    }
                    else
                    {
                        sql = $@"select * from Package
                                 where package_type like N'%{clue}%'";  //利用like %% 查找含有關鍵字{clue}之資料
                    }
                }
                return new Response(200, "查詢成功", true, db.NewQuery<Package>(sql));
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Package/add")]
        public Response addPackage(Package package)
        {
            try
            {
                string checksql = $"select * from Package where package_type=N'{package.package_type}'";
                List<Package> isAdded = db.NewQuery<Package>(checksql);
                if (isAdded.Count > 0 && !isAdded[0].isDelete)
                {
                    return new Response(400, "產品已存在", false);
                }
                else
                {
                    db.Insert<Package>(package);
                    return new Response(200, "新增成功", true);
                }
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPut]
        [Route("api/Package/delete")]
        public Response deletePackage(string package_ID)
        {
            try
            {
                string sql = $"select * from Package where package_ID='{package_ID}'";
                Package package = db.NewQuery<Package>(sql).FirstOrDefault();
                package.isDelete = true;
                db.Update<Package>(package);

                return new Response(200, "刪除成功", true);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Package/edit")]
        public Response editPackage(Package editpackage)
        {
            try
            {
                string sql = $"select * from Package where package_ID='{editpackage.package_ID}'";
                Package package = db.NewQuery<Package>(sql).FirstOrDefault();

                package.package_type = editpackage.package_type;
                package.price = editpackage.price;

                db.Update<Package>(package);

                return new Response(200, "編輯成功!", true);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false); //5開頭 --> Server端錯誤 
            }
        }

        [HttpGet]
        [Route("api/Package/getRelatedProduct")]
        public Response getRelatedProduct(string package_ID)
        {
            try
            {
                string sql = $@"select * from Commodity
                                inner join Bridge
                                on Commodity.commodity_ID = Bridge.Commodity_ID
                                where package_ID='{package_ID}'";
                List<Commodity> produts = db.NewQuery<Commodity>(sql);

                return new Response(200, "", true, produts);
            }catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Package/relatedPackageProducts")]
        public Response relatedPackageProducts(RelatedProducts relatedProducts)
        {
            try
            {
                string[] names = relatedProducts.names;
                string sql = $@"select * from Commodity
                                where 1=1 and";
                for(int i=0; i<names.Length; i++)
                {
                    if(i == names.Length-1)
                    {
                        sql += $" (name = N'{names[i]}')";
                    }else
                    {
                        sql += $" (name = N'{names[i]}') or";
                    }                 
                }

                List<Commodity> want_related_products = db.NewQuery<Commodity>(sql);
            
                string checksql = $"select * from Bridge where package_ID='{relatedProducts.package_ID}'";
                List<Bridge> checklist = db.NewQuery<Bridge>(checksql);

                Bridge bridge = new Bridge();
                bridge.package_ID = relatedProducts.package_ID;

                
                if (checklist.Count > 0) //update 已經有關聯商品
                {                        
                    for(int i=0; i<want_related_products.Count; i++)
                    {
                        int checkcount = 0;
                        for (int j=0; j<checklist.Count; j++)
                        {
                            if (!want_related_products.ElementAt(i).commodity_ID.ToString().Equals(checklist.ElementAt(j).commodity_ID.ToString())) //不存在此商品
                            {
                                checkcount++;
                            }else
                            {
                                break;
                            }
                        }
                        if(checkcount == checklist.Count)
                        {
                            bridge.commodity_ID = want_related_products.ElementAt(i).commodity_ID;
                            db.Insert<Bridge>(bridge);
                        }
                    }
                }else//add 未有關聯商品
                {                   
                    for (int i = 0; i < want_related_products.Count; i++)
                    {
                        bridge.commodity_ID = want_related_products.ElementAt(i).commodity_ID;
                        db.Insert<Bridge>(bridge);
                    }
                }
                
                return new Response(200, "更新成功！", true);
            }
            catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
           
        }

        [HttpPost]
        [Route("api/Package/deleteRelatedProduct")]
        public Response deleteRelatedProduct(RelatedProducts delete_related_products) //直接刪除 不用isDelete
        {
            try
            {
                string sql = $@"select * from Bridge 
                                where package_ID='{delete_related_products.package_ID}' and";
                string c_idsql = $@"select * from Commodity where 1=1 and";

                for(int i=0; i < delete_related_products.names.Length; i++)
                {
                    if(i == delete_related_products.names.Length-1)
                    {
                        c_idsql += $" name=N'{delete_related_products.names[i]}'";
                    }else
                    {
                        c_idsql += $" name=N'{delete_related_products.names[i]}' or";
                    }
                }
                List<Commodity> commodities = db.NewQuery<Commodity>(c_idsql);

                for (int i = 0; i < commodities.Count; i++)
                {
                    if (i == commodities.Count - 1)
                    {
                        sql += $" commodity_ID='{commodities.ElementAt(i).commodity_ID}'";
                    }
                    else
                    {
                        sql += $" commodity_ID='{commodities.ElementAt(i).commodity_ID}' or";
                    }
                }

                List<Bridge> bridges = db.NewQuery<Bridge>(sql);             
                foreach (Bridge bridge in bridges)
                {
                    db.Delete<Bridge>(bridge);
                }
                return new Response(200, "刪除成功!", true);
                             
            }
            catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }
    }
}
