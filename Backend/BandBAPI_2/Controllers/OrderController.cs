﻿using BandBAPI_2.Models;
using BandBAPI_2.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web.Http;

namespace BandBAPI_2.Controllers
{
    public class OrderController : ApiController
    {
        BaseDB db = new BaseDB();
        [HttpGet]
        [Route("api/Order/getOrderlist")]
        public Response getOrderlist()
        {
            try
            {
                List<OrderList> lists = new List<OrderList>();
                OrderList list;

                string sql = @"select * from [Order] 
                                where isDelete = 0
                                order by CONVERT(datetime, createTime) desc";
                List<Order> orders = db.NewQuery<Order>(sql);

                foreach(Order order in orders)
                {
                    list = new OrderList(); 
                
                    string sql2 = $"select * from Customer where customer_ID='{order.customer_ID}'";
                    Customer customer = db.NewQuery<Customer>(sql2).FirstOrDefault();

                    list.order = order;
                    list.customer = customer;

                    lists.Add(list);                  
                }
                

                return new Response(200, "成功回傳訂單列表!", true, lists);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Order/addOrder")]
        public Response addOrder(addOrder add_order) //有更改資料庫 2021/07/06
        {
            try
            {
                string sql = $@"select * from Customer where name=N'{add_order.order_Info.customer_name}' and phone_number='{add_order.order_Info.phone}'";
                Customer cust = new Customer();
                if (db.NewQuery<Customer>(sql).Count > 0)
                {
                    cust = db.NewQuery<Customer>(sql).FirstOrDefault();
                }else
                {
                    return new Response(500, "尚未存在此客戶，請先新增客戶！", false);
                }

                string datepattern = "MM-dd-yyyy HH:mm:ss";
                string order_number = "OR";

                var taiwan_calendar = new System.Globalization.TaiwanCalendar();
                DateTime now = DateTime.Now;
                var year = taiwan_calendar.GetYear(DateTime.Now).ToString();

                //group by SUBSTRING(createTime,1,10)
                string sql2 = $@"select COUNT(*) as 'count' from [Order]
                                 where '{now.ToString("MM-dd-yyyy")}' = SUBSTRING(createTime,1,10)";
                string count = (Int32.Parse(db.NewQuery<Count>(sql2).FirstOrDefault().count)+1).ToString("d2");
                    
                order_number += year + now.Month.ToString("d2") + now.Day.ToString("d2") + count;

                Order order = new Order();                
                order.customer_ID = cust.customer_ID;
                order.total_money = add_order.order_Info.total_money;
                order.deposit = add_order.order_Info.deposit;
                order.createTime = DateTime.Now.ToString(datepattern);
                order.order_number = order_number;

                order.people_num = add_order.order_Info.people_num;
                order.cash_income = add_order.order_Info.cash_income;
                order.additional_purchase = add_order.order_Info.additional_purchase;

                order.deposit_date = add_order.order_Info.deposit_date;

                order.checkinDate = add_order.order_Info.checkinDate;

                db.Insert<Order>(order);

                sql = "select * from [Order] order by CONVERT(datetime,  createTime) desc"; //descending sort / ascending sort
                Order neworder = db.NewQuery<Order>(sql).FirstOrDefault();

                string sql3 = "";
                int price = 0;
                for (int i = 0; i<add_order.details_Info.Count; i++)
                {
                    Details_Info info = add_order.details_Info.ElementAt(i);
                    Order_Detail detail = new Order_Detail();
                    if(!info.isPackage && String.IsNullOrEmpty(info.product)) //兩個都無
                    {
                        continue;
                    }else if(info.isPackage)
                    {
                        detail.products_ID = Guid.Parse(info.package);
                        detail.productType = 2; //2為package
                        sql3 = $"select * from Package where package_ID = '{info.package}'";
                        Package p = db.NewQuery<Package>(sql3).FirstOrDefault();
                        price = p.price;

                    }else if(!info.isPackage && !String.IsNullOrEmpty(info.product))
                    {
                        detail.products_ID = Guid.Parse(info.product);
                        detail.productType = 1; //1為product
                        sql3 = $"select * from Commodity where commodity_ID = '{info.product}'";
                        Commodity c = db.NewQuery<Commodity>(sql3).FirstOrDefault();
                        price = c.price;
                    }
                  
                    detail.order_ID = neworder.order_ID;
                    detail.price = price;
                    detail.num = Int32.Parse(add_order.details_Info.ElementAt(i).num);
                    detail.orderdate = DateTime.Now.ToString(datepattern);

                    db.Insert<Order_Detail>(detail);
                }                        
                
                return new Response(200, "新增成功!", true);
                
            }catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
        } 

        [HttpPost]
        [Route("api/Order/deleteOrder")] 
        public Response deleteOrder(string o_id)
        {
            try
            {
                string sql = $"select * from [Order] where order_ID='{o_id}'";
                Order order = db.NewQuery<Order>(sql).FirstOrDefault();
                order.isDelete = true;

                string sql2 = $"select * from Order_Detail where order_ID='{o_id}'";
                List<Order_Detail> details = db.NewQuery<Order_Detail>(sql2);
                foreach(Order_Detail detail in details)
                {
                    detail.isDelete = true;
                    db.Update(detail);
                }

                db.Update<Order>(order);

                return new Response(200, "刪除成功!", true);

            }catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpGet]
        [Route("api/Order/searchOrder")]
        public Response searchOrder(string clue)
        {
            try
            {
                List<OrderList> lists = new List<OrderList>();

                string sql = $@"select * from (select * from [Order] where isDelete = 0) as [Order]
                                inner join Customer 
                                on Customer.customer_ID = [Order].customer_ID
                                where 1=1"; //Order為sql關鍵字 []表

                Regex regex = new Regex("^OR[0-9]+$");
                if (regex.IsMatch(clue))
                {
                    sql += $" and order_number like '%{clue}%'";
                }else
                {
                    sql += $" and name like N'%{clue}%'";
                }

                List<Order> orders = db.NewQuery<Order>(sql);
                OrderList list;
                foreach (Order order in orders)
                {
                    string sql2 = $"select * from Customer where customer_ID='{order.customer_ID}'";
                    Customer customer = db.NewQuery<Customer>(sql2).FirstOrDefault();

                    list = new OrderList(); //call by reference
                    list.order = order;
                    list.customer = customer;

                    lists.Add(list);
                }

                return new Response(200, "查詢成功", true, lists);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Order/editOrder")]
        public Response updateOrder(EditOrder editOrder)
        {
            try
            {
                string sql = $"select * from [Order] where order_number='{editOrder.ordernumber}'";
                Order order = db.NewQuery<Order>(sql).FirstOrDefault();
                order.total_money = editOrder.total_money;
                order.deposit = editOrder.deposit;
                order.people_num = editOrder.people_num;
                order.deposit_date = editOrder.deposit_date;

                string sql2 = $"select * from Customer where name = N'{editOrder.customer_name}' and phone_number = '{editOrder.phone}'";
                List<Customer> customer = db.NewQuery<Customer>(sql2);
                if(customer.Count > 0)
                {
                    order.customer_ID = customer.ElementAt(0).customer_ID;
                    db.Update<Order>(order);
                }else
                {
                    return new Response(500, "查無此客戶，請先新增客戶！", false);
                }               

                return new Response(200, "更改成功!", true);

            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Order/getOrderInfo")]
        public Response getOrderInfo(string order_number)
        {
            try
            {
                //上面一包 Object
                string sql_order_cust = $@"
                    select 
                    order_ID, order_number, total_money, deposit, people_num, cash_income, additional_purchase, deposit_date, checkinDate, [Order].createTime,
                    name, phone_number, Line_id
                    from (select * from [Order] where order_number = '{order_number}') as [Order]
                    inner join Customer 
                    on [Order].customer_ID = Customer.customer_ID
                ";
                
                OrderUpperInfo upperInfo = db.NewQuery<OrderUpperInfo>(sql_order_cust).FirstOrDefault();

                //下面一包 List 
                string sql_details = $@"select * from Order_Detail where order_ID='{upperInfo.order_ID}'";
                List<Order_Detail> details = db.NewQuery<Order_Detail>(sql_details);

                List<OrderLowerInfo> lowerInfos = new List<OrderLowerInfo>();
                for (int i = 0; i < details.Count; i++)
                {
                    OrderLowerInfo lowerInfo = new OrderLowerInfo();
                    lowerInfo.num = details.ElementAt(i).num;

                    string sql_products = "";
                    if (details.ElementAt(i).productType == 1) //commodity
                    {
                        sql_products = $"select * from Commodity where commodity_ID = '{details.ElementAt(i).products_ID}'";
                        Commodity commodity = db.NewQuery<Commodity>(sql_products).FirstOrDefault();

                        lowerInfo.product_type = 1;
                        lowerInfo.products_name = commodity.name;
                        lowerInfo.products_ID = commodity.commodity_ID;
                        lowerInfo.unit_price = commodity.price;                      
                    }
                    else
                    {
                        sql_products = $"select * from Package where package_ID = '{details.ElementAt(i).products_ID}'";
                        Package package = db.NewQuery<Package>(sql_products).FirstOrDefault();

                        lowerInfo.product_type = 2;
                        lowerInfo.products_name = package.package_type;
                        lowerInfo.products_ID = package.package_ID;
                        lowerInfo.unit_price = package.price;
                    }
                    lowerInfo.total_money = lowerInfo.unit_price * lowerInfo.num;

                    lowerInfos.Add(lowerInfo);
                }

                //合併
                Order_Detail_Info detail_Info = new Order_Detail_Info();
                detail_Info.upperInfo = upperInfo;
                detail_Info.lowerInfos = lowerInfos;

                return new Response(200, "成功獲取細項！", true, detail_Info);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Order/updateOrderDetailInfo")]
        public Response updateOrderDetailInfo(Order_Detail_Info detail_Info)
        {
            try
            {
                string sql_check_cust = $"select * from Customer where name=N'{detail_Info.upperInfo.name}' and phone_number='{detail_Info.upperInfo.phone_number}' and Line_id='{detail_Info.upperInfo.Line_id}'";
                List<Customer> customers = db.NewQuery<Customer>(sql_check_cust);
                if (customers.Count > 0)
                {
                    string sql_target_order = $"select * from [Order] where order_ID = '{detail_Info.upperInfo.order_ID}'";
                    Order order = db.NewQuery<Order>(sql_target_order).FirstOrDefault();
                    order.customer_ID = customers.ElementAt(0).customer_ID;
                    order.total_money = detail_Info.upperInfo.total_money;
                    order.deposit = detail_Info.upperInfo.deposit;
                    order.people_num = detail_Info.upperInfo.people_num;
                    order.cash_income = detail_Info.upperInfo.cash_income;
                    order.additional_purchase = detail_Info.upperInfo.additional_purchase;
                    order.deposit_date = detail_Info.upperInfo.deposit_date;
                    order.checkinDate = detail_Info.upperInfo.checkinDate;
                    db.Update<Order>(order);
              
                    string sql_details = $"select * from Order_Detail where order_ID = '{detail_Info.upperInfo.order_ID}'";
                    List<Order_Detail> details = db.NewQuery<Order_Detail>(sql_details);
                    for (int i = 0; i < details.Count; i++) //先刪除
                    {
                        db.Delete<Order_Detail>(details.ElementAt(i)); 
                    }

                    string datepattern = "MM-dd-yyyy HH:mm:ss";
                    for (int i=0; i<detail_Info.lowerInfos.Count; i++) //再重新新增
                    {
                        Order_Detail detail = new Order_Detail();
                        detail.order_ID = detail_Info.upperInfo.order_ID;
                        detail.products_ID = detail_Info.lowerInfos.ElementAt(i).products_ID;
                        detail.productType = detail_Info.lowerInfos.ElementAt(i).product_type;
                        detail.price = detail_Info.lowerInfos.ElementAt(i).unit_price;
                        detail.num = detail_Info.lowerInfos.ElementAt(i).num;                 
                        detail.orderdate = DateTime.Now.ToString(datepattern); 

                        db.Insert<Order_Detail>(detail);
                    }

                    return new Response(200, "更新成功!", true);
                }
                else
                {
                    return new Response(500, "不存在此客戶！", false);
                }
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }
    }
}
