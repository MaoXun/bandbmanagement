﻿using BandBAPI_2.Models;
using BandBAPI_2.Utility;
using Newtonsoft.Json;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Script.Serialization;

namespace BandBAPI_2.Controllers
{
    public class ReportController : ApiController
    {
        BaseDB db = new BaseDB();
        JavaScriptSerializer js = new JavaScriptSerializer();

        [HttpPost]
        [Route("api/Report/getRangeReport")]
        public Response getRangeReport(string firstdate, string lastdate)
        {
            try
            {
                //search bt createTime
                //string sql = $@"select * from [Order]
                //                where CONVERT(datetime,  createTime) > CONVERT(datetime, '{firstdate}')  
                //                and CONVERT(datetime,  createTime) < CONVERT(datetime, '{lastdate} 23:59:59')  
                //                and isDelete = 0
                //                order by CONVERT(datetime,  createTime) desc";


                //search by checkinDate
                string sql = $@"select * from [Order]
                                where convert(date, checkinDate) >= convert(date, '{firstdate}') 
                                and convert(date, checkinDate) <= convert(date, '{lastdate}') 
                                order by convert(date, checkinDate) desc";

                List<Order> orders = db.NewQuery<Order>(sql);

                if (orders.Count == 0)
                {
                    return new Response(500, "此區間內尚無訂單!", false);
                }

                return new Response(200, "查詢成功", true, orders);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Report/getAnalysis")]
        public Response getAnalysis(string year, string month) //一次撈 銷貨成本 銷貨費用 利潤分析 股東分紅
        {
            try
            {
                string sql = $"select * from CostReport where year='{year}' and month='{month}' ";
                List<CostReport> reports = db.NewQuery<CostReport>(sql); //進一次資料庫

                CostReport cost_of_good = reports.Where(x => x.type.Equals("CostOfGood")).ToList().FirstOrDefault(); //銷貨成本  
                CostReport selling_cost = reports.Where(x => x.type.Equals("SellingCost")).ToList().FirstOrDefault(); //銷貨費用                 

                sql = $"select * from ProfitReport where year='{year}' and month='{month}'";
                ProfitReport profit = db.NewQuery<ProfitReport>(sql).FirstOrDefault();  //利潤分析 

                sql = $"select * from ShareHolder where year='{year}' and month='{month}'";
                ShareHolder share = db.NewQuery<ShareHolder>(sql).FirstOrDefault();

                AnalysisReport analysisReport = new AnalysisReport();
                analysisReport.cost_of_good = cost_of_good;
                analysisReport.selling_cost = selling_cost;
                analysisReport.profit = profit;
                analysisReport.share = share;

                return new Response(200, "", true, analysisReport);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Report/addCost")]
        public Response addCost(CostReport addReport) //type-> 銷貨成本 銷貨費用 //開新資料表
        {
            try
            {
                string sql = $"select * from CostReport where year='{addReport.year}' and month='{addReport.month}' and type='{addReport.type}'";
                List<CostReport> reports = db.NewQuery<CostReport>(sql);
                if (reports.Count > 0) //已存在本月份報表
                {
                    string t = "";
                    if (addReport.type.Equals("CostOfGood"))
                    {
                        t = "銷貨成本";
                    } else if (addReport.type.Equals("SellingCost"))
                    {
                        t = "銷貨費用";
                    }
                    return new Response(500, $"已存在本月份{t}報表! 請查詢並編輯!", false);
                }

                //前端已stringfy --> 直接把jsondata存入資料庫
                db.Insert<CostReport>(addReport);

                string sqlprofit = $"select * from CostReport where year='{addReport.year}' and month='{addReport.month}'";
                List<CostReport> costs = db.NewQuery<CostReport>(sqlprofit);
                if (costs.Count == 2)
                {
                    //可製作利潤分析
                    string sqlvalue = $@"select * from [Order]
                                         where CONVERT(datetime,  createTime) > CONVERT(datetime, '{addReport.month} 01 {addReport.year}')  
                                         and CONVERT(datetime,  createTime) < CONVERT(datetime, '{addReport.month} {DateTime.DaysInMonth(Int32.Parse(addReport.year), Int32.Parse(addReport.month))} {addReport.year} 23:59:59')  
                                         and isDelete = 0
                                         order by CONVERT(datetime,  createTime) desc";
                    List<Order> orders = db.NewQuery<Order>(sqlvalue);
                    int valueofbusiness = 0;
                    for (int i = 0; i < orders.Count; i++)
                    {
                        valueofbusiness += orders.ElementAt(i).total_money;
                    }

                    int costofgood = costs.Find(cost => cost.type.Equals("CostOfGood")).cost_money;
                    int sellingcost = costs.Find(cost => cost.type.Equals("SellingCost")).cost_money;

                    Dictionary<string, int> profitdict = new Dictionary<string, int>();
                    profitdict.Add("營業額", valueofbusiness);
                    profitdict.Add("銷貨成本", costofgood);
                    profitdict.Add("銷貨費用", sellingcost);
                    profitdict.Add("營運淨利", valueofbusiness - costofgood - sellingcost);
                    string profitstr = MyDictToJsonString(profitdict);



                    ProfitReport profit = new ProfitReport();
                    profit.year = addReport.year;
                    profit.month = addReport.month;
                    profit.jsonprofit = profitstr;

                    db.Insert<ProfitReport>(profit);

                    return new Response(200, "新增成功! 已製作完利潤分析報表!", true);
                } else
                {
                    return new Response(200, "新增成功!", true);
                }
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Report/addShareHolder")]
        public Response addShareHolder(ShareHolder addshare)
        {
            try
            {
                string sqlcheck = $"select * from ShareHolder where year='{addshare.year}' and month='{addshare.month}'";
                List<ShareHolder> shares = db.NewQuery<ShareHolder>(sqlcheck);
                if (shares.Count > 0)
                {
                    return new Response(500, "已在本月份股東分紅報表 請查詢並編輯！", false);
                }

                db.Insert<ShareHolder>(addshare);
                return new Response(200, "新增成功", true);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Report/editAnalysisReport")]
        public Response editAnalysisReport(EditAnalysis edit_analysis)
        {
            try
            {
                Dictionary<string, string> map = new Dictionary<string, string>();
                map.Add("銷貨成本", "CostOfGood");
                map.Add("銷貨費用", "SellingCost");
                map.Add("利潤分析", "ProfitReport");
                map.Add("股東分紅", "ShareHolder");

                string type = "";
                foreach (KeyValuePair<string, string> pair in map)
                {
                    if (pair.Key == edit_analysis.edittype)
                    {
                        type = pair.Value;
                        break;
                    }
                }

                //直接update json string 欄位

                //取得待編輯物件
                string sql = "";
                dynamic willedit; //動態型別
                if (type.Equals("CostOfGood") || type.Equals("SellingCost"))
                {
                    sql = $"select * from CostReport where type='{type}' and year='{edit_analysis.year}' and month='{edit_analysis.month}'";
                    willedit = db.NewQuery<CostReport>(sql).FirstOrDefault();
                    if (type.Equals("CostOfGood"))
                    {
                        willedit.jsondata = edit_analysis.costofgooddata;
                    } else
                    {
                        willedit.jsondata = edit_analysis.sellingcostdata;
                    }
                    willedit.cost_money = edit_analysis.costtotal;
                    db.Update<CostReport>(willedit);

                    if (!edit_analysis.profitdata.Equals("[]")) //有利潤分析
                    {
                        sql = $"select * from ProfitReport where year='{edit_analysis.year}' and month='{edit_analysis.month}'";
                        willedit = db.NewQuery<ProfitReport>(sql).FirstOrDefault();
                        willedit.jsonprofit = edit_analysis.profitdata;

                        db.Update<ProfitReport>(willedit);
                    }
                }
                else
                {
                    sql = $"select * from {type} where year='{edit_analysis.year}' and month='{edit_analysis.month}'";
                    if(type == "ShareHolder")
                    {
                        willedit = db.NewQuery<ShareHolder>(sql).FirstOrDefault();
                        willedit.jsonshare = edit_analysis.sharedata;
                        db.Update<ShareHolder>(willedit);
                    }else if(type == "ProfitReport")
                    {
                        willedit = db.NewQuery<ProfitReport>(sql).FirstOrDefault();
                        willedit.jsonprofit = edit_analysis.profitdata;
                        db.Update<ProfitReport>(willedit);
                    }
                    

                }


                return new Response(200, "編輯成功!", true);
            } catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Report/exportExcel")]
        public Response exportExcel(string first, string last, string year, string month)
        {
            string base64 = "";
            try
            {
                if (!Directory.Exists(@"C:\ExportExcel")) //不存在指定資料夾
                {
                    Directory.CreateDirectory(@"C:\ExportExcel");
                }

                DirectoryInfo dir = new DirectoryInfo(@"C:\ExportExcel");
                FileInfo file;
                int index = 0;
                if (!File.Exists(@"C:\ExportExcel\Report_0.xlsx")) //尚未有任何報表
                {
                    file = new FileInfo(@"C:\ExportExcel\Report_0.xlsx");
                }
                else
                {
                    foreach (var f in dir.GetFiles())
                    {
                        if (f.Name.Substring(0, 6).Equals("Report") && Path.GetExtension($@"C:\ExportExcel\{f.Name}").Equals(".xlsx"))
                        {
                            index = Math.Max(index, Int32.Parse(f.Name.Split('.')[0].Substring(7)) + 1);
                        }
                    }
                    file = new FileInfo($@"C:\ExportExcel\Report_{index}.xlsx");
                }

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage ep = new ExcelPackage())
                {
                    ep.Workbook.Worksheets.Add($"Sheet{index}");
                    ExcelWorksheet sheet = ep.Workbook.Worksheets[$"Sheet{index}"];

                    List<Order> excel_orders = (List<Order>)getRangeReport(first, last).data;
                    AnalysisReport analysis = (AnalysisReport)getAnalysis(year, month).data;

                    //json to dict
                    Dictionary<string, int> costOfGood = new Dictionary<string, int>();
                    Dictionary<string, int> sellingCost = new Dictionary<string, int>();
                    Dictionary<string, int> profit = new Dictionary<string, int>();
                    Dictionary<string, float> share = new Dictionary<string, float>();
                    string mes = "";

                    if (year.Equals("novalue") && month.Equals("novalue"))
                    {
                        mes = "但分析報表尚未選擇年月份!";
                    }
                    else
                    {
                        costOfGood = analysis.cost_of_good == null ? costOfGood : JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<string, int>>>(analysis.cost_of_good.jsondata).ToDictionary(x => x.Key, x => x.Value);
                        sellingCost = analysis.selling_cost == null ? sellingCost : JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<string, int>>>(analysis.selling_cost.jsondata).ToDictionary(x => x.Key, x => x.Value);
                        profit = analysis.profit == null ? profit : JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<string, int>>>(analysis.profit.jsonprofit).ToDictionary(x => x.Key, x => x.Value);
                        share = analysis.share == null ? share : JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<string, float>>>(analysis.share.jsonshare).ToDictionary(x => x.Key, x => x.Value);
                    }

                    int anaCount = costOfGood.Count + sellingCost.Count + profit.Count + share.Count;
                    int orderCount = excel_orders.Count;
                    int reportHeight = Math.Max(anaCount, orderCount);

                    //訂單報表
                    setExcelOrderReport(ref sheet, excel_orders, reportHeight);

                    sheet.Row(1).Height = 30;
                    sheet.Cells[1, 2].Value = "你我的相遇3D創意民宿";
                    sheet.Cells[1, 2, 1, 4].Merge = true;
                    sheet.Cells[1, 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Cells[1, 2].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheet.Cells[1, 2].Style.Font.Bold = true;
                    sheet.Cells[1, 2].Style.Font.Name = "微軟正黑體";
                    sheet.Cells[1, 2].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    sheet.Cells[1, 4].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    sheet.Cells[1, 2].Style.Border.Left.Color.SetColor(Color.Yellow);
                    sheet.Cells[1, 4].Style.Border.Right.Color.SetColor(Color.Yellow);

                    sheet.Cells[1, 5].Value = string.Format("{0}年{1}月{2}日 ~ {3}年{4}月{5}日 訂單報表", first.Split('-')[0], first.Split('-')[1], first.Split('-')[2], last.Split('-')[0], last.Split('-')[1], last.Split('-')[2]);
                    sheet.Cells[1, 5, 1, 10].Merge = true;
                    sheet.Cells[1, 5].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    sheet.Cells[1, 5].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    sheet.Cells[1, 5].Style.Font.Bold = true;
                    sheet.Cells[1, 5].Style.Font.Name = "微軟正黑體";
                    sheet.Cells[1, 10].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    sheet.Cells[1, 10].Style.Border.Right.Color.SetColor(Color.Yellow);

                    //分析報表
                    if (costOfGood.Count > 0 && sellingCost.Count > 0 && profit.Count > 0 && share.Count > 0)
                    {
                        setExcelAnalysisReport(ref sheet, costOfGood, sellingCost, profit, share, reportHeight, anaCount);
                        sheet.Cells[1, 11].Value = string.Format("{0}年{1}月營業報表", year, month);
                        sheet.Cells[1, 11, 1, 12].Merge = true;
                        sheet.Cells[1, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        sheet.Cells[1, 11].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                        sheet.Cells[1, 11].Style.Font.Bold = true;
                        sheet.Cells[1, 11].Style.Font.Name = "微軟正黑體";
                        sheet.Cells[1, 12].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        sheet.Cells[1, 12].Style.Border.Right.Color.SetColor(Color.Yellow);
                    }
                    else
                    {
                        if (mes.Equals(""))
                        {
                            mes = "但有分析報表未新增!";
                        }

                        ep.SaveAs(file); 

                        base64 = fileToBase64String(file); //將file物件轉成Base64 string ， file是一個FileInfo物件
                        File.Delete(file.FullName); //在Server端中刪除檔案
                        return new Response(200, "報表以產出 "+mes, true, base64);
                    }

                    ep.SaveAs(file);
                }

                base64 = fileToBase64String(file);
                File.Delete(file.FullName);
                return new Response(200, "報表已產出!", true, base64);

            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Report/searchRangeProfit")]
        public Response searchRangeProfit(string first, string last)
        {
            try
            {
                string sql = $@"select * from ProfitReport
                                where year >= '{first.Split('-')[0]}'  
                                and  year <=  '{last.Split('-')[0]}'
                                and  month >= '{first.Split('-')[1]}' 
                                and month <= '{last.Split('-')[1]}'
                                order by year asc, month asc";
                List<ProfitReport> profits = db.NewQuery<ProfitReport>(sql);

                return new Response(200, "查詢成功!", true, profits);
            }
            catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        public string MyDictToJsonString(Dictionary<string, int> dict)
        {         
            var eachstrings = dict.Select(x => string.Format("{{\"key\":\"{0}\"comma\"value\":{1}}}", x.Key,x.Value)); //Select方法 lambda表達式 // /"表示雙引號 {{表示左大括號
            string result = "[" + string.Join(",", eachstrings).Replace("comma", ",") + "]"; //用replace替換comma 因為在string.format中 不能放, 會混淆

            return result;
        }

        public void setExcelOrderReport(ref ExcelWorksheet sheet, List<Order> orders, int count)
        {
            //從B2開始 到 J2
            string[] titles = {"入住日期", "星期", "訂金匯入日期", "訂金(匯款)", "人數", "現金收入", "加價構", "現金盤點", "營業額" };          
            for (int j = 0; j < 9; j++)
            {
                sheet.Cells[2, j + 2].Value = titles[j];
                sheet.Column(j + 2).Width = 4 * (titles[j].Length);     
                sheet.Cells[2, j + 2].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                sheet.Cells[2, j + 2].Style.Border.Top.Style = ExcelBorderStyle.Double;
                sheet.Cells[2, j + 2].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                sheet.Cells[2, j + 2].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                sheet.Cells[2, j + 2].Style.Border.Left.Style = ExcelBorderStyle.Thin;

                if (j == 0)
                {
                    sheet.Cells[2, j + 2].Style.Border.Left.Style = ExcelBorderStyle.Double;
                }else if(j == 8)
                {
                    sheet.Cells[2, j + 2].Style.Border.Right.Style = ExcelBorderStyle.Double;
                }
            }

            int[] sum = new int[9];
            for (int i = 0; i < orders.Count; i++)
            {
                //訂單創建日期
                //sheet.Cells[i + 3, 2].Value = getSimpleDate(orders.ElementAt(i).createTime).Substring(0, 5);
                //sheet.Cells[i + 3, 3].Value = DateTime.Parse(getSimpleDate(orders.ElementAt(i).createTime), CultureInfo.InvariantCulture).ToString("ddd").Substring(1, 1);

                //入住日期
                sheet.Cells[i + 3, 2].Value = string.IsNullOrEmpty(orders.ElementAt(i).checkinDate) ? "-" : getSimpleDate2(orders.ElementAt(i).checkinDate).Substring(0, 5);
                sheet.Cells[i + 3, 3].Value = string.IsNullOrEmpty(orders.ElementAt(i).checkinDate) ? "-" : DateTime.Parse(getSimpleDate2(orders.ElementAt(i).checkinDate)).ToString("ddd", new CultureInfo("zh-TW")).Substring(1, 1);

                sheet.Cells[i + 3, 4].Value = orders.ElementAt(i).deposit_date.Equals("") ? "-" : string.Format("{0}月{1}日", orders.ElementAt(i).deposit_date.Substring(5, 2), orders.ElementAt(i).deposit_date.Substring(8, 2));

                sheet.Cells[i + 3, 5].Value = orders.ElementAt(i).deposit;
                sum[3] += orders.ElementAt(i).deposit;

                sheet.Cells[i + 3, 6].Value = orders.ElementAt(i).people_num;
                sum[4] += orders.ElementAt(i).people_num;

                sheet.Cells[i + 3, 7].Value = orders.ElementAt(i).cash_income;
                sum[5] += orders.ElementAt(i).cash_income;

                sheet.Cells[i + 3, 8].Value = orders.ElementAt(i).additional_purchase;
                sum[6] += orders.ElementAt(i).additional_purchase;

                sheet.Cells[i + 3, 9].Value = orders.ElementAt(i).cash_income + orders.ElementAt(i).additional_purchase;
                sum[7] += orders.ElementAt(i).cash_income + orders.ElementAt(i).additional_purchase;

                sheet.Cells[i + 3, 10].Value = orders.ElementAt(i).total_money;
                sum[8] += orders.ElementAt(i).total_money;
            }

            sheet.Cells[2 + count + 6, 2].Value = "合計:";
            sheet.Cells[2 + count + 6, 2, 2 + count + 6, 4].Merge = true;

            for(int j=3; j<9; j++)
            {
                sheet.Cells[2 + count + 6, j + 2].Value = sum[j];               
            }
            sheet.Cells[2 + count + 6, 10].Style.Font.Color.SetColor(Color.Red);

            //原本: orders.Count+2+1  後來: count+6(包含合計)
            sheet.Cells[2, 2, 2+count+6, 10].Style.Font.Name = "微軟正黑體"; //+6是標題和合計 若只產訂單報表也沒差
            sheet.Cells[2, 2, 2+count+6, 10].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            sheet.Cells[2, 2, 2+count+6, 10].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Row(2).Height = 20;
            for (int i=3; i<= 2 + count + 6; i++)
            {
                sheet.Row(i).Height = 20;
                for (int j=2; j<11; j++)
                {
                    sheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    if(j == 2)
                    {
                        sheet.Cells[i, j].Style.Border.Left.Style = ExcelBorderStyle.Double;
                    }else if(j == 10)
                    {
                        sheet.Cells[i, j].Style.Border.Right.Style = ExcelBorderStyle.Double;
                    }

                    if(i == 2 + count + 6)
                    {
                        sheet.Cells[i, j].Style.Border.Bottom.Style = ExcelBorderStyle.Double; 
                    }
                }
            }
        }

        public void setExcelAnalysisReport(ref ExcelWorksheet sheet, Dictionary<string, int> costOfGood, Dictionary<string, int> sellingCost, Dictionary<string,int> profit, Dictionary<string, float> share, int count, int anaCount)
        {
            setExcelSingleAnalysis(ref sheet, costOfGood, "銷貨成本", count, anaCount, 2);
            setExcelSingleAnalysis(ref sheet, sellingCost, "銷貨費用", count, anaCount, costOfGood.Count + 2 + 2);
            setExcelSingleAnalysis(ref sheet, profit, "利潤分析", count, anaCount, costOfGood.Count + sellingCost.Count + 2 + 2 + 2);
            setExcelSingleAnalysis(ref sheet, share, "股東分紅", count, anaCount, costOfGood.Count + sellingCost.Count + profit.Count + 2 + 2 + 2 + 1, profit["營運淨利"]);
        }

        public void setExcelSingleAnalysis(ref ExcelWorksheet sheet, Dictionary<string, int> report, string title, int count, int anaCount, int startrow, int businessValue = 0)
        {
            //標題
            sheet.Cells[startrow, 11].Value = title;
            sheet.Cells[startrow, 11, startrow, 12].Merge = true;
            sheet.Cells[startrow, 11, startrow, 12].Style.Border.BorderAround(ExcelBorderStyle.Double);
            sheet.Cells[startrow, 11, startrow, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //內容
            dynamic sum = 0;
            for (int i = 0; i < report.Count; i++)
            {
                sheet.Cells[startrow + 1 + i, 11].Value = title.Equals("股東分紅") ? report.ElementAt(i).Key +" ("+ report.ElementAt(i).Value.ToString()+"%" +") " : report.ElementAt(i).Key;
                sheet.Cells[startrow + 1 + i, 12].Value = title.Equals("股東分紅") ? ((double)report.ElementAt(i).Value/100)*businessValue : report.ElementAt(i).Value; // int / int --> int
                sum += title.Equals("股東分紅") ? ((double)report.ElementAt(i).Value / 100) * businessValue : report.ElementAt(i).Value; 
            }

            int deltaCount = count > anaCount ? count - anaCount : 0;
            int rowCount = 0;

            //Style
            if (title.Equals("銷貨費用")) //若 count > anaCount 把多的行數加在銷貨費用
            {
                rowCount = report.Count + deltaCount;
            }
            else
            {
                rowCount = report.Count;
            }

            sheet.Cells[startrow + 1 + rowCount, 11].Value = "合計";
            sheet.Cells[startrow + 1 + rowCount, 12].Value = sum;
            sheet.Cells[startrow, 11, startrow + rowCount + 1, 12].Style.Font.Name = "微軟正黑體";
            sheet.Cells[startrow, 11, startrow + rowCount + 1, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[startrow, 11, startrow + rowCount + 1, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            sheet.Row(startrow).Height = 20;
            sheet.Column(11).Width = 30;
            sheet.Column(12).Width = 30;
            for (int i = startrow + 1; i <= startrow + 1 + rowCount; i++)
            {
                sheet.Row(i).Height = 20;    
                for (int j = 11; j <= 12; j++)
                {
                    sheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    if (j == 11)
                    {
                        sheet.Cells[i, j].Style.Border.Left.Style = ExcelBorderStyle.Double;
                        if(i == startrow + rowCount+1)
                        {
                            sheet.Cells[i, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                    else if (j == 12)
                    {
                        sheet.Cells[i, j].Style.Border.Right.Style = ExcelBorderStyle.Double;
                        if (i == startrow + rowCount+1)
                        {
                            sheet.Cells[i, j].Style.Font.Color.SetColor(Color.Red);
                        }
                    }

                    if(i== startrow + rowCount+1)
                    {
                        sheet.Cells[i, j].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                    }
                }
            }
        }
        public void setExcelSingleAnalysis(ref ExcelWorksheet sheet, Dictionary<string, float> report, string title, int count, int anaCount, int startrow, int businessValue = 0)
        {
            //標題
            sheet.Cells[startrow, 11].Value = title;
            sheet.Cells[startrow, 11, startrow, 12].Merge = true;
            sheet.Cells[startrow, 11, startrow, 12].Style.Border.BorderAround(ExcelBorderStyle.Double);
            sheet.Cells[startrow, 11, startrow, 12].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

            //內容
            dynamic sum = 0;
            for (int i = 0; i < report.Count; i++)
            {
                sheet.Cells[startrow + 1 + i, 11].Value = title.Equals("股東分紅") ? report.ElementAt(i).Key + " (" + report.ElementAt(i).Value.ToString() + "%" + ") " : report.ElementAt(i).Key;
                sheet.Cells[startrow + 1 + i, 12].Value = title.Equals("股東分紅") ? (report.ElementAt(i).Value / 100) * businessValue : report.ElementAt(i).Value; 
                sum += title.Equals("股東分紅") ? (report.ElementAt(i).Value / 100) * businessValue : report.ElementAt(i).Value;
            }

            int deltaCount = count > anaCount ? count - anaCount : 0;
            int rowCount = 0;

            //Style
            if (title.Equals("銷貨費用")) //若 count > anaCount 把多的行數加在銷貨費用
            {
                rowCount = report.Count + deltaCount;
            }
            else
            {
                rowCount = report.Count;
            }

            sheet.Cells[startrow + 1 + rowCount, 11].Value = "合計";
            sheet.Cells[startrow + 1 + rowCount, 12].Value = sum;
            sheet.Cells[startrow, 11, startrow + rowCount + 1, 12].Style.Font.Name = "微軟正黑體";
            sheet.Cells[startrow, 11, startrow + rowCount + 1, 12].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            sheet.Cells[startrow, 11, startrow + rowCount + 1, 12].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
            sheet.Row(startrow).Height = 20;
            sheet.Column(11).Width = 30;
            sheet.Column(12).Width = 30;
            for (int i = startrow + 1; i <= startrow + 1 + rowCount; i++)
            {
                sheet.Row(i).Height = 20;
                for (int j = 11; j <= 12; j++)
                {
                    sheet.Cells[i, j].Style.Border.BorderAround(ExcelBorderStyle.Thin);
                    if (j == 11)
                    {
                        sheet.Cells[i, j].Style.Border.Left.Style = ExcelBorderStyle.Double;
                        if (i == startrow + rowCount + 1)
                        {
                            sheet.Cells[i, j].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                        }
                    }
                    else if (j == 12)
                    {
                        sheet.Cells[i, j].Style.Border.Right.Style = ExcelBorderStyle.Double;
                        if (i == startrow + rowCount + 1)
                        {
                            sheet.Cells[i, j].Style.Font.Color.SetColor(Color.Red);
                        }
                    }

                    if (i == startrow + rowCount + 1)
                    {
                        sheet.Cells[i, j].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                    }
                }
            }
        }


        private static string getSimpleDate(string createTime)
        {
            string[] times = createTime.Split(' ');
            string year = times[2];
            times = times.Where(x => x.Equals(times[1]) || x.Equals(times[0])).ToArray();
            string simpledate = string.Join("/", times);
            return simpledate + "/" + year;
        }

        private static string getSimpleDate2(string checkinDate)
        {
            string[] times = checkinDate.Split('-');
            string year = times[0];
            times = times.Where(x => x.Equals(times[1]) || x.Equals(times[2])).ToArray();
            string simpledate = string.Join("/", times);

            return simpledate + "/" + year;
        }

        private static string fileToBase64String(FileInfo file)
        {
            string base64 = String.Empty;
            var pathName = file.FullName;
            byte[] docBytes = null;

            using (StreamReader strm = new StreamReader(pathName, System.Text.Encoding.UTF8))
            {
                Stream s = strm.BaseStream;
                BinaryReader r = new BinaryReader(s);
                docBytes = r.ReadBytes(Convert.ToInt32(r.BaseStream.Length));
                base64 = Convert.ToBase64String(docBytes);
                r.Close();
                s.Close();
                strm.Close();
            }

            return base64;
        }
    }
}
