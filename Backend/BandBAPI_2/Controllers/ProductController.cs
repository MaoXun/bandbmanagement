﻿using BandBAPI_2.Models;
using BandBAPI_2.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace BandBAPI_2.Controllers
{
    public class ProductController : ApiController
    {
        BaseDB db = new BaseDB();

        [HttpGet]
        [Route("api/Product/list")]
        public Response getProductsList()
        {
            try
            {
                string sql = "select * from Commodity";
                return new Response(200, "成功回傳商品列表!", true, db.NewQuery<Commodity>(sql));
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }

        }

        [HttpGet]
        [Route("api/Product/search")]
        public Response productSearch(string clue)
        {
            try
            {
                string sql = "";
                try
                {
                    sql = $@"select * from Commodity
                            where commodity_ID='{Guid.Parse(clue)}'";
                }
                catch
                {
                    string pattern = "^[0-9]+$"; //開頭 結尾 都需是數字
                    Regex regex = new Regex(pattern);
                    if (regex.IsMatch(clue))
                    {
                        sql = $@"select * from Commodity
                                 where price={clue} or cost={clue}";
                    }
                    else
                    {
                        sql = $@"select * from Commodity 
                                 where name like N'%{clue}%'";  //利用like %% 查找含有關鍵字{clue}之資料
                    }
                }
                return new Response(200, "查詢成功", true, db.NewQuery<Commodity>(sql));
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Product/add")]
        public Response addProduct(Commodity commodity)
        {
            try
            {
                string checksql = $"select * from Commodity where name=N'{commodity.name}'";
                List<Commodity> isAdded = db.NewQuery<Commodity>(checksql);
                if (isAdded.Count > 0 && !isAdded[0].isDelete)
                {
                    return new Response(400, "產品已存在", false);
                }
                else
                {
                    db.Insert<Commodity>(commodity);
                    return new Response(200, "新增成功", true);
                }
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPut]
        [Route("api/Product/delete")]
        public Response deleteProduct(string commodity_ID)
        {
            try
            {
                string sql = $"select * from Commodity where commodity_ID='{commodity_ID}'";
                Commodity commodity = db.NewQuery<Commodity>(sql).FirstOrDefault();
                commodity.isDelete = true;
                db.Update<Commodity>(commodity);

                return new Response(200, "刪除成功", true);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Product/edit")]
        public Response editProduct(Commodity editcommodity)
        {
            try
            {
                string sql = $"select * from Commodity where commodity_ID='{editcommodity.commodity_ID}'";
                Commodity commodity = db.NewQuery<Commodity>(sql).FirstOrDefault();

                commodity.name = editcommodity.name;
                commodity.price = editcommodity.price;
                commodity.cost = editcommodity.cost;

                db.Update<Commodity>(commodity);

                return new Response(200, "編輯成功!", true);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false); //5開頭 --> Server端錯誤 
            }
        }


        [HttpPost]
        [Route("api/Product/{productID}/uploadFiles")]
        public Response uploadFiles(String productID)
        {
            var httpRequest = HttpContext.Current.Request; //取得當前httpRequest所有資訊
            if (httpRequest.Files.Count < 1)
            {
                return new Response(500, "檔案不可為空!", false);
            }
            if (!Directory.Exists(HttpContext.Current.Server.MapPath($"~/Image/Product/{productID}")))
            {

                Directory.CreateDirectory(HttpContext.Current.Server.MapPath($"~/Image/Product/{productID}")); //由Server.MapPath 利用~ 取得'當前'根目錄
            }
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                var filePath = HttpContext.Current.Server.MapPath($"~/Image/Product/{productID}/" + postedFile.FileName);

                string path = $@"C:/inetpub/wwwroot/BandBManagement/Image/Product/{productID}";
                string[] files = Directory.GetFiles(path);
                int count = 0;
                int maxnum = 1;
                foreach (string img in files) //判斷是否有已存在img
                {
                    string test = img.Substring(0, img.Length - 7) + ".jpg";
                    if (filePath.Equals(img) || filePath.Equals(test))
                    {
                        string numstr = img.Substring(img.Length - 6, 1); //取得數字字串
                        int localmax;
                        if (Int32.TryParse(numstr, out localmax)) //out使參數由reference傳遞
                        {
                            maxnum = Math.Max(maxnum, localmax);
                        }
                        count++;
                    }
                }
                if (count > 0) //有重複檔案 且有可能刪了原始檔案 只留下重複檔案
                {
                    filePath = filePath.Remove(filePath.Length - 4, 4) + "(" + (maxnum + 1) + ")" + ".jpg";
                    postedFile.SaveAs(filePath);
                }
                else
                {
                    postedFile.SaveAs(filePath);
                }
                // NOTE: To store in memory use postedFile.InputStream
            }

            return new Response(200, "上傳成功!", true);
        }

        [HttpGet]
        [Route("api/Product/{productID}/getImageBase64")]
        public Response productImagesBase64(string productID)
        {
            try
            {
                ImageAndBase64 transformer = new ImageAndBase64();
                Dictionary<string, string> imgs = new Dictionary<string, string>();

                //List<string> base64s = new List<string>();
                string folder_name = $@"C:/inetpub/wwwroot/BandBManagement/Image/Product/{productID}"; //絕對路徑

                foreach (string file_name in System.IO.Directory.GetFiles(folder_name))
                {
                    Image img = Image.FromFile(file_name);
                    imgs.Add(file_name, transformer.ImageToBase64(img, System.Drawing.Imaging.ImageFormat.Jpeg));
                    img.Dispose();
                    //base64s.Add(transformer.ImageToBase64(Image.FromFile(file_name), System.Drawing.Imaging.ImageFormat.Jpeg));
                }
                return new Response(200, "", true, imgs);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost] //用Get Delete data內不能放東西 
        [Route("api/Product/deleteImage")]
        public Response productDeleteImage(ProductDeleteImage product_image)
        {
            try
            {
                //string path = $@"C:/Users/Einstein/Source/Repos/BandBAPI_2/BandBAPI_2/App_Data/Image/Product/{product_image.product_ID}/";
                //string[] files = Directory.GetFiles(path);

                foreach (string image in product_image.will_delete_images)
                {
                    File.Delete(image);
                }


                return new Response(200, "刪除成功!", true, product_image.will_delete_images);
            }
            catch (Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }
    }
}
