﻿using BandBAPI_2.Models;
using BandBAPI_2.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace BandBAPI_2.Controllers
{
    public class CustomerController : ApiController
    {
        BaseDB db = new BaseDB();

        [HttpGet]
        [Route("api/Customer/list")]
        public Response getCustomers()
        {         
            try
            {
                string sql = "select * from Customer order by CONVERT(datetime, createTime)  desc";
                List<Customer> list = db.NewQuery<Customer>(sql);

                return new Response(200, "成功回傳顧客列表!", true, list);
            }
            catch(Exception e) //若後端有問題才不會死當 且若傳回有問題的資料回去 也能傳送錯誤訊息 而不是在前端直接alert("成功!")
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpGet]
        [Route("api/Customer/search")]
        public Response getSpecificCustomers(string clue)
        {
            try
            {
                string sql = "";
                try
                {
                    sql = $@"select * from Customer 
                            where customer_ID='{Guid.Parse(clue)}'";
                }
                catch
                {
                    sql = $@"select * from Customer 
                            where name like N'%{clue}%' or phone_number like '%{clue}%'or Line_id like N'%{clue}%'";
                }

                List<Customer> list = db.NewQuery<Customer>(sql);

                return new Response(200, "查詢成功", true, list);
            }
            catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        [HttpPost]
        [Route("api/Customer/add")]
        public Response addCustomer(Customer addCustomer)
        {  
            try
            {
                Customer customer = new Customer();
                customer.name = addCustomer.name;
                customer.phone_number = addCustomer.phone_number;
                customer.Line_id = addCustomer.Line_id;
                customer.email = addCustomer.email;
                customer.isDelete = false;

                string datepattern = "MM-dd-yyyy HH:mm:ss";
                customer.createTime = DateTime.Now.ToString(datepattern);

                string isAddedsql = $@"select * from Customer where
                                    name=N'{customer.name}' and phone_number='{customer.phone_number}'";
                List<Customer> isAdded = db.NewQuery<Customer>(isAddedsql);
                if(isAdded.Count > 0 && !isAdded[0].isDelete)
                {
                    return new Response(400, "客戶已存在!", false);
                }else
                {
                    db.Insert<Customer>(customer);
                    return new Response(200, "新增成功!", true);
                }
            }
            catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }
        }

        //[HttpDelete]
        //[Route("api/Customer/delete")]
        //public void deleteCustomer(string customer_ID)
        //{
        //    Customer customer = new Customer();
        //    customer.customer_ID = Guid.Parse(customer_ID);
            
        //    db.Delete<Customer>(customer); //Delete函數只找PK      
        //}

        //刪除改用isDelete做
        [HttpPut]
        [Route("api/Customer/delete")]
        public Response deleteCustomer(string customer_ID)
        {
            try
            {
                string sql = $"select * from Customer where customer_ID='{customer_ID}'";
                Customer customer = db.NewQuery<Customer>(sql).FirstOrDefault();
                customer.isDelete = true;
                db.Update<Customer>(customer);

                return new Response(200, "刪除成功!", true);
            }
            catch(Exception e)
            {
                return new Response(500, e.Message, false);
            }       
        }

        [HttpPost]
        [Route("api/Customer/edit")]
        public Response editCustomer(Customer editCustomer)
        {
            try
            {
                string sql = $"select * from Customer where customer_ID='{editCustomer.customer_ID}'";
                Customer customer = db.NewQuery<Customer>(sql).FirstOrDefault();

                customer.name = editCustomer.name;
                customer.phone_number = editCustomer.phone_number;
                customer.Line_id = editCustomer.Line_id;
                customer.email = editCustomer.email;

                db.Update<Customer>(customer);

                return new Response(200, "編輯成功!", true);
            }
            catch(Exception e)
            {
                return new Response(500, e.Message, false); //5開頭 --> Server端錯誤 
            }           
        }

       
    }
}
