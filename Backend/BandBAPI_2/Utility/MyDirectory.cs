﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Utility
{
    public class MyDirectory
    {
        public static string getParentDirectoryPath(string path, int level)
        {
            string result = path;
            for(int i=0; i<level; i++)
            {
                result = System.IO.Directory.GetParent(result).ToString();
            }
            return result;
        }
    }
}