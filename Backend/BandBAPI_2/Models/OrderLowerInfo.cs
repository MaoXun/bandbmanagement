﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class OrderLowerInfo
    {
        public string products_name { get; set; }
        public Guid products_ID { get; set; }
        public int product_type { get; set; }
        public int unit_price { get; set; }
        public int num { get; set; }
        public int total_money { get; set; }
    }
}