﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Details_Info
    {
        public string package { get; set; }
        public string product { get; set; }
        public string num { get; set; }
        public bool isPackage { get; set; }
    }
}