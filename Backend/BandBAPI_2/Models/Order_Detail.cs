﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Order_Detail
    {
        public Guid order_detail_ID { get; set; }
        public Guid products_ID { get; set; }
        public Guid order_ID { get; set; }
        public int productType { get; set; }
        public int price { get; set; }
        public string orderdate { get; set; }
        public int num { get; set; }
        public bool isDelete { get; set; }
    }
}