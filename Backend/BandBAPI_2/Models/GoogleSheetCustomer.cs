﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class GoogleSheetCustomer
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string LineID { get; set; }
        public string email { get; set; }
    }
}