﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Package
    {
        public Guid package_ID { get; set; }
        public string package_type { get; set; }
        public int price { get; set;}
        public bool isDelete { get; set; }
    }
}