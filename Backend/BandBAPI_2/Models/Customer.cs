﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Customer
    {
        public Guid customer_ID { get; set; }
        public string phone_number { get; set; }
        public string Line_id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public bool isDelete { get; set; }
        public string createTime { get; set; } //宣告成string比較不會有轉型上的錯誤
        public string line_user_id { get; set; }
    }
}