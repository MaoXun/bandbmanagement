﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Commodity
    {
        public Guid commodity_ID { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public int cost { get; set; }
        public bool isDelete { get; set; }
    }
}