﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class EditOrder
    {
        public string ordernumber { get; set; }
        public int total_money { get; set; }
        public int deposit { get; set; }
        public string customer_name { get; set; }
        public string phone { get; set; }
        public int people_num { get; set; }
        public string deposit_date { get; set; }
    }
}