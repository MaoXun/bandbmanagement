﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Response
    {
        public int code { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public object data { get; set; }

        public Response(int code, string message, bool success) //新增 刪除 修改
        {
            this.code = code;
            this.message = message;
            this.success = success;
        }

        public Response(int code, string message, bool success, object data) //查詢
        {
            this.code = code;
            this.message = message;
            this.success = success;
            this.data = data;
        }
    }
}