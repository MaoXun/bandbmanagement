﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Bridge
    {
        public Guid bridge_ID { get; set; }
        public Guid package_ID { get; set; }
        public Guid commodity_ID { get; set; }
    }
}