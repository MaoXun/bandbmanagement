﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class OrderList
    {
        public Order order { get; set; }
        public Customer customer { get; set; }
    }
}