﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class ShareHolder
    {
        public Guid share_ID { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public string jsonshare { get; set; }
    }
}