﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class OrderUpperInfo
    {
        public Guid order_ID { get; set; }
        public string order_number { get; set; }
        public int total_money { get; set; }
        public int deposit { get; set; }
        public int people_num { get; set; }
        public int cash_income { get; set; }
        public int additional_purchase { get; set; }
        public string createTime { get; set; }

        public string name { get; set; }
        public string phone_number { get; set; }
        public string Line_id { get; set; }

        public string deposit_date { get; set; }

        public string checkinDate { get; set; }
    }
}