﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class CostReport
    {
        public Guid cost_ID { get; set; }
        public string type { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public string jsondata { get; set; }
        public int cost_money { get; set; }
    }
}