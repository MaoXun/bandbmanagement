﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Order_Info
    {
        public string customer_name { get; set; }
        public string phone { get; set; }
        public int total_money { get; set; }
        public int deposit { get; set; }

        public int people_num { get; set; }
        public int cash_income { get; set; }
        public int additional_purchase { get; set; }

        public string deposit_date { get; set; }

        public string checkinDate { get; set; }
    }
}