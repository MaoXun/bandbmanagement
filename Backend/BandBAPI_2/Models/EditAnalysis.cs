﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class EditAnalysis
    {
        public string costofgooddata { get; set; }
        public string sellingcostdata { get; set; }
        public string profitdata { get; set; }
        public string sharedata { get; set; }
        public string edittype { get; set; }
        public string year { get; set; }
        public string month { get; set; }
        public int costtotal { get; set; }
    }
}