﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class Order_Detail_Info
    {
        public OrderUpperInfo upperInfo { get; set; }
        public List<OrderLowerInfo> lowerInfos { get; set; }
    }
}