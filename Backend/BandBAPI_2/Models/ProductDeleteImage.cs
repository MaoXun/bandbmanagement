﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class ProductDeleteImage
    {
        public string product_ID { get; set; }
        public string[] will_delete_images { get; set; }
    }
}