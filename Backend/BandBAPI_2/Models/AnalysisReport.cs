﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BandBAPI_2.Models
{
    public class AnalysisReport
    {
        public CostReport cost_of_good { get; set; }
        public CostReport selling_cost { get; set; }
        public ProfitReport profit { get; set; }
        public ShareHolder share { get; set; }
    }
}